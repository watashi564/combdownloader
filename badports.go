package main

const ALLOW = false

func is_bad_port(port uint16) bool {
	switch port {
	// unknown port handling
	default:
		return ALLOW // unknown port, let it go
	// app specific ports
	case 8333:
		return ALLOW // Bitcoin main net
	case 18333:
		return ALLOW // Bitcoin test net
	case 18444:
		return ALLOW // Bitcoin reg test net
	// blocklist - app specific
	case 2121:
		return !ALLOW // Haircomb core wallet UI
	case 8332:
		return !ALLOW // Comb downloader RPC / Bitcoin RPC
	case 18332:
		return !ALLOW // Comb downloader Test RPC / Bitcoin Testnet RPC
	case 80:
		return !ALLOW // HTTP
	case 443:
		return !ALLOW // HTTPS
	case 18443:
		return !ALLOW // Bitcoin reg test net rpc
	// blocklist - from public sources
	case 10080:
		return !ALLOW //amanda
	case 101:
		return !ALLOW //hostname
	case 102:
		return !ALLOW //iso-tsap
	case 103:
		return !ALLOW //gppitnp
	case 104:
		return !ALLOW //acr-nema
	case 109:
		return !ALLOW //pop2
	case 110:
		return !ALLOW //pop3
	case 111:
		return !ALLOW //sunrpc
	case 113:
		return !ALLOW //auth
	case 115:
		return !ALLOW //sftp
	case 117:
		return !ALLOW //uucp-path
	case 119:
		return !ALLOW //nntp
	case 11:
		return !ALLOW //systat
	case 123:
		return !ALLOW //NTP
	case 135:
		return !ALLOW //loc-srv /epmap
	case 137:
		return !ALLOW //netbios/netbios-ns
	case 139:
		return !ALLOW //netbios/netbios-ssn
	case 13:
		return !ALLOW //daytime
	case 143:
		return !ALLOW //imap/imap2
	case 15:
		return !ALLOW //netstat
	case 161:
		return !ALLOW //snmp
	case 1719:
		return !ALLOW //h323gatestat
	case 1720:
		return !ALLOW //h323hostcall
	case 1723:
		return !ALLOW //pptp
	case 179:
		return !ALLOW //BGP
	case 17:
		return !ALLOW //qotd
	case 19:
		return !ALLOW //chargen
	case 1:
		return !ALLOW //tcpmux
	case 2049:
		return !ALLOW //nfs
	case 20:
		return !ALLOW //ftp data
	case 21:
		return !ALLOW //ftp
	case 22:
		return !ALLOW //ssh
	case 23:
		return !ALLOW //telnet
	case 25:
		return !ALLOW //smtp
	case 3659:
		return !ALLOW //apple-sasl / PasswordServer
	case 37:
		return !ALLOW //time
	case 389:
		return !ALLOW //ldap
	case 4045:
		return !ALLOW //lockd/npp
	case 427:
		return !ALLOW //afp (alternate)/SLP (Also used by Apple Filing Protocol)/svrloc
	case 42:
		return !ALLOW //name
	case 43:
		return !ALLOW //nicname
	case 465:
		return !ALLOW //smtp (alternate)/smtp+ssl/submissions
	case 5060:
		return !ALLOW //sip
	case 5061:
		return !ALLOW //sips
	case 512:
		return !ALLOW //print / exec
	case 513:
		return !ALLOW //login
	case 514:
		return !ALLOW //shell
	case 515:
		return !ALLOW //printer
	case 526:
		return !ALLOW //tempo
	case 530:
		return !ALLOW //courier
	case 531:
		return !ALLOW //chat
	case 532:
		return !ALLOW //netnews
	case 53:
		return !ALLOW //domain
	case 540:
		return !ALLOW //uucp
	case 548:
		return !ALLOW //AFP (Apple Filing Protocol)
	case 554:
		return !ALLOW //rtsp
	case 556:
		return !ALLOW //remotefs
	case 563:
		return !ALLOW //nntp+ssl/nntps
	case 587:
		return !ALLOW //smtp (rfc6409)/smtp (outgoing)/submission
	case 6000:
		return !ALLOW //X11
	case 601:
		return !ALLOW //syslog-conn/syslog-conn (rfc3195)
	case 636:
		return !ALLOW //ldaps/ldap+ssl
	case 6566:
		return !ALLOW //sane-port
	case 6665:
		return !ALLOW //Alternate IRC [Apple addition]/ircu
	case 6666:
		return !ALLOW //Alternate IRC [Apple addition]/ircu
	case 6667:
		return !ALLOW //Standard IRC [Apple addition]/ircu
	case 6668:
		return !ALLOW //Alternate IRC [Apple addition]/irc (alternate)/ircu
	case 6669:
		return !ALLOW //Alternate IRC [Apple addition]/irc (alternate)/ircu
	case 6697:
		return !ALLOW //IRC + TLS/ircs-u
	case 69:
		return !ALLOW //tftp
	case 77:
		return !ALLOW //priv-rjs
	case 79:
		return !ALLOW //finger
	case 7:
		return !ALLOW //echo
	case 87:
		return !ALLOW //ttylink
	case 95:
		return !ALLOW //supdup
	case 989:
		return !ALLOW //ftps-data
	case 990:
		return !ALLOW //ftps
	case 993:
		return !ALLOW //imaps/imap+ssl/ldap+ssl
	case 995:
		return !ALLOW //pop3s/pop3+ssl
	case 9:
		return !ALLOW //discard
	}
}