package main

import "github.com/btcsuite/btcd/wire"
import "errors"
import "net"

const UserAgentName = "Satoshi"
const UserAgentVersion = "24.0.1"

func NewMsgVersionFromConn(conn net.Conn, nonce uint64,
	lastBlock int32, sf wire.ServiceFlag) *wire.MsgVersion {

	// Don't assume any services until we know otherwise.
	lna := wire.NewNetAddress(conn.LocalAddr().(*net.TCPAddr), sf)

	// Don't assume any services until we know otherwise.
	rna := wire.NewNetAddress(conn.RemoteAddr().(*net.TCPAddr), sf)

	_ = errNoMsgVersion

	var ver = wire.NewMsgVersion(lna, rna, nonce, lastBlock)
	ver.Services = sf
	return ver
}

const ServiceCombNodeClient = uint64(wire.ServiceFlag(1 << 10))
const ServiceCombClient = uint64(wire.SFNodeWitness|wire.SFNodeNetwork) | ServiceCombNodeClient
const ServiceCombNodeBloomClient = uint64(wire.SFNodeBloom) | ServiceCombNodeClient
const ServiceCombNode = uint64(wire.SFNodeWitness|wire.SFNodeNetwork) | ServiceCombNodeBloomClient

var errNoMsgVersion = errors.New("err_no_msg_version")
var errBCash = errors.New("err_bcash")
var errCheckpoint = errors.New("err_checkpoint_not_reached")
var errBlkMerkleNotValid = errors.New("err_block_merkle_not_valid")
var errChainShortened = errors.New("chain_shortened")
var errPingNoResponse = errors.New("ping_no_response")
var errBloomForced = errors.New("bloom_forced")
var errChainInactive = errors.New("chain_inactive")
var errBadGetHeaders = errors.New("bad_get_headers")
var errCombNodeForced = errors.New("comb_node_forced")