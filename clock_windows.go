//go:build windows
// +build windows

package main

import "runtime"
import "syscall"

func init() {
	// set up 1ms sleeper in windows
	if runtime.GOOS == "windows" {
		var winmmDLL = syscall.NewLazyDLL("winmm.dll")
		var procTimeBeginPeriod = winmmDLL.NewProc("timeBeginPeriod")
		procTimeBeginPeriod.Call(uintptr(1))
	}
}