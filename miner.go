package main

import "bitbucket.org/watashi564/accelerator/libbloom"
import "bitbucket.org/watashi564/accelerator/libfile"
import "time"
import "log"
import "math/rand"

func miner_should_be_running() bool {
	return ConfigMinerDifficulty != 0 && ConfigMinerFunctions != 0
}

func miner() {
	if !miner_should_be_running() {
		return
	}

	var fully_synced = false

	for {

		if !myChainIsActive() {
			time.Sleep(time.Second)
			continue
		}

		if 0 != acceleratorCount(1) {
			log.Printf("[MINER]: reorg")
			time.Sleep(time.Second)
			continue
		}

		var hash, height, file = acceleratorTop()
		if height == 0 {
			log.Printf("[MINER]: accelerators not synced")
			time.Sleep(time.Second)
			continue
		}

		// download txmap and p2wsh
		var blockhashCH, ok = myGetHash(height)
		if !ok {
			if !fully_synced {
				fully_synced = true
				log.Printf("[MINER]: Fully synced %d %X\n", height-1, hash)
			}
			time.Sleep(time.Second)
			continue
		}
		fully_synced = false

		var blockhash = [32]byte(blockhashCH)
		for i := 0; i < 16; i++ {
			blockhash[i], blockhash[31-i] = blockhash[31-i], blockhash[i]
		}

		var block = PullBlock(blockhash)
		if block == nil {
			log.Printf("[MINER]: block could not have been downloaded")
			continue
		}
		log.Printf("[MINER]: Downloaded and mining %d %x\n", height, blockhash)
		var txmap = make(map[[32]byte]bool)
		var p2wsh uint64
		for _, tx := range block.Tx {
			var txid = tx.TxId
			txmap[txid] = false
			for _, out := range tx.Vout {
				if out.ScriptPubKey != nil && out.ScriptPubKey.Type == "witness_v0_scripthash" {
					txmap[txid] = true
					p2wsh++
				}
			}
		}

		if p2wsh == 0 {
			if err := acceleratorAttach(Accelerator{
				Hash:      hash,
				BlockHash: blockhash,
				Count:     p2wsh,
			}); err != nil {
				log.Printf("[MINER]: could not add new empty accelerator")
				continue
			}
			continue
		}

		tweakadd := rand.Uint32()

		size, tweak := libbloom.Search(byte(ConfigMinerDifficulty), tweakadd, uint32(2*p2wsh), uint32(ConfigMinerFunctions), txmap)
		if size == 0 {
			log.Printf("[MINER]: nothing was mined, please raise the mining difficulty")
			continue
		}
		fp, bloom := libbloom.Test(size, tweak, uint32(ConfigMinerFunctions), txmap)
		if fp != 0 {
			log.Printf("[MINER]: imperfect bloom")
			continue
		}

		err := file.Add(&libfile.BloomFilter{
			BlockHash:  blockhash,
			P2wshCount: p2wsh,
			Payload:    libbloom.Serialize(bloom, uint32(ConfigMinerFunctions), tweak),
		})
		if err != nil {
			log.Printf("[MINER]: write error")
			continue
		}
		if err := acceleratorAttach(Accelerator{
			Hash:      hash,
			BlockHash: blockhash,
			Count:     p2wsh,
		}); err != nil {
			log.Printf("[MINER]: could not add new accelerator")
			continue
		}
	}
}