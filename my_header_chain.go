package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "github.com/btcsuite/btcd/wire"
import "sync"

var myMutex sync.RWMutex
var myChain []chainhash.Hash
var myActiveChain *[]chainhash.Hash
var myBitsChain []uint32
var myTimeChain []uint32
var myTime2Chain []uint32
var myHeight map[chainhash.Hash]uint64
var myTotal, myActiveTotal uint64
var myActiveTotalView *uint64
var myVerTimeNonceIfServer [][3]uint32
var myMerkleRootsIfServer []chainhash.Hash
var myTestNetExtraBitsChain []uint32

func myResetBitsTime() {
	myBitsChain = myBitsChain[:0]
	myBitsChain = append(myBitsChain, uint32(0x1d00ffff))
	myTestNetExtraBitsChain = myTestNetExtraBitsChain[:0]
	myTestNetExtraBitsChain = append(myTestNetExtraBitsChain, uint32(0x1d00ffff))
	myTimeChain = myTimeChain[:0]
	myTimeChain = append(myTimeChain, uint32(1231006505))
	myTime2Chain = myTime2Chain[:0]
	myTime2Chain = append(myTime2Chain, uint32(1231006505))
}

func myTruncateBitsTimes(height uint64) {
	if ConfigTestnet() {
		myTestNetExtraBitsChain = myTestNetExtraBitsChain[:height]
	} else {
		myBitsChain = myBitsChain[:(height+2016)/2016]
	}
	myTimeChain = myTimeChain[:(height+2017)/2016]
	myTime2Chain = myTime2Chain[:(height+2016)/2016]
}

func init() {
	myHeight = make(map[chainhash.Hash]uint64)
	myChain = make([]chainhash.Hash, 0, 1000000)
	myActiveChain = nil
	myBitsChain = make([]uint32, 0, 500)
	myTimeChain = make([]uint32, 0, 500)
	myTime2Chain = make([]uint32, 0, 500)
	myTestNetExtraBitsChain = make([]uint32, 0, 1000000)
	myResetBitsTime()
}

func myCheckpoint() (bh *chainhash.Hash) {
	var cpoint = ConfigCheckpoint()
	if len(cpoint) != 64 {
		return nil
	}

	h, err := chainhash.NewHashFromStr(cpoint)
	if err != nil {
		return nil
	}
	return h
}

func myPut(bh *chainhash.Hash, height uint64) {
	myMutex.Lock()
	if height == 0 {
		myHeight[*bh] = 0
	}
	if height == myTotal {
		myChain = append(myChain, *bh)
	}
	myMutex.Unlock()
}
func myServer() {
	myMutex.Lock()
	myMerkleRootsIfServer = make([]chainhash.Hash, 0, 1000000)
	myVerTimeNonceIfServer = make([][3]uint32, 0, 1000000)
	myMutex.Unlock()
}

func myChainIsActive() bool {
	myMutex.Lock()
	var is = myActiveChain == &myChain
	myMutex.Unlock()
	return is
}
func myChainIsInitiallyActive() bool {
	myMutex.Lock()
	var is = myActiveChain != nil
	myMutex.Unlock()
	return is
}
func myGetHash(height uint64) (hash chainhash.Hash, ok bool) {
	myMutex.RLock()
	if uint64(len(myChain)) > height {
		hash = myChain[height]
		ok = true
	}
	myMutex.RUnlock()
	return
}

func myGetAll(height uint64) (version, time, nonce, bits uint32, mklroot, prev, ths chainhash.Hash) {
	myMutex.Lock()
	if (height == 0) || (uint64(len(myVerTimeNonceIfServer)) < height) ||
		(uint64(len(myMerkleRootsIfServer)) < height) ||
		(uint64(len(myChain)) < height+1) {
		myMutex.Unlock()
		return
	}
	var data = myVerTimeNonceIfServer[height-1]
	version = data[0]
	time = data[1]
	nonce = data[2]

	if ConfigTestnet() {
		bits = myTestNetExtraBitsChain[height]
	} else {
		bits = myBitsChain[height/2016]
	}
	mklroot = myMerkleRootsIfServer[height-1]
	prev = myChain[height-1]
	ths = myChain[height]
	myMutex.Unlock()
	return
}

func myCount() (cnt uint64) {
	myMutex.RLock()
	cnt = myTotal
	myMutex.RUnlock()
	return cnt
}

func myTop() (bh *chainhash.Hash, height uint64) {
	myMutex.RLock()
	height = uint64(len(myChain) - 1)
	bh = &myChain[height]
	myMutex.RUnlock()
	return bh, height
}

func myHas(bh *chainhash.Hash) bool {
	myMutex.RLock()
	var h, has = myHeight[*bh]
	if has && h > 0 && h <= myTotal {
		has = myChain[h] == *bh
	} else {
		has = false
	}
	myMutex.RUnlock()
	return has
}

// mutex must be taken
func myBits() uint32 {
	return myBitsChain[len(myBitsChain)-1]
}

// mutex must be taken
func myTime() uint32 {
	return myTimeChain[len(myTimeChain)-1]
}

// mutex must be taken
func myTime2() uint32 {
	return myTime2Chain[len(myTime2Chain)-1]
}

//func myBitsHeight(height uint64) uint32 {
//	return myBitsChain[height/2016]
//}

// mutex must be taken
func myDoTimestamps(timestamp uint32) {
	if myTotal%2016 == 2015 {
		myTime2Chain = append(myTime2Chain, timestamp)
	} else if myTotal%2016 == 2014 {
		myTimeChain = append(myTimeChain, timestamp)
	}
}

// mutex must be taken
func myDoRetarget(bh *wire.BlockHeader) bool {
	if ConfigTestnet() {
		myTestNetExtraBitsChain = append(myTestNetExtraBitsChain, bh.Bits)
	} else if myTotal%2016 == 2015 {
		var nextBits = CalculateNextWorkRequired(myTime2(), myTime(), myBits())
		myBitsChain = append(myBitsChain, nextBits)
	}
	myDoTimestamps(uint32(bh.Timestamp.Unix()))
	if ConfigTestnet() || ConfigRegtest {
		return true
	}
	return bh.Bits == myBits()
}

func myThrowAwayTopBlocks(depth uint64) {

	myMutex.Lock()
	defer myMutex.Unlock()

	if myTotal < depth {
		myTotal = 0
		myChain = myChain[:1]

		if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
			myMerkleRootsIfServer = make([]chainhash.Hash, 0, 1000000)
			myVerTimeNonceIfServer = make([][3]uint32, 0, 1000000)
		}
	} else {
		myTotal -= depth
		myChain = myChain[:myTotal+1]

		if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
			myVerTimeNonceIfServer = myVerTimeNonceIfServer[:myTotal]
			myMerkleRootsIfServer = myMerkleRootsIfServer[:myTotal]
		}
	}

	myTruncateBitsTimes(myTotal)

	if uint64(len(myChain)) != myTotal+1 {
		println("chain length inconsistent")
		return
	}
}

func myAttach(bh *wire.BlockHeader, checkpoint *chainhash.Hash, checkHeight *uint64, reorg func(a, b *chainhash.Hash, x, y uint64)) bool {
	hash := bh.BlockHash()

	if !IsDifficultBlock(&hash, bh.Bits) {
		return false
	}

	myMutex.Lock()
	defer myMutex.Unlock()

	if uint64(len(myChain)) != myTotal+1 {
		println("chain length inconsistent")
		return false
	}

	var reorg1height = myHeight[bh.PrevBlock]
	if reorg1height >= ConfigMaxBlock {
		return true
	}
	if reorg1height > 0 && reorg1height < myTotal {

		if myChain[reorg1height+1] != hash {

			myChain = myChain[:reorg1height+1]
			if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
				myVerTimeNonceIfServer = myVerTimeNonceIfServer[:reorg1height]
				myMerkleRootsIfServer = myMerkleRootsIfServer[:reorg1height]
			}
			myTotal = uint64(len(myChain) - 1)
			myTruncateBitsTimes(myTotal)
			reorg(&bh.PrevBlock, &hash, reorg1height, reorg1height+1)

			// if reorg below checkpoint, make it non visited
			if checkHeight != nil && myTotal < *checkHeight {
				*checkHeight = 0
			}
		}
	}

	current := &myChain[myTotal]
	if *current == bh.PrevBlock {

		if !myDoRetarget(bh) {
			return false
		}

		myTotal++
		myChain = append(myChain, hash)
		if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
			myVerTimeNonceIfServer = append(myVerTimeNonceIfServer, [3]uint32{uint32(bh.Version), uint32(bh.Timestamp.Unix()), uint32(bh.Nonce)})
			myMerkleRootsIfServer = append(myMerkleRootsIfServer, bh.MerkleRoot)
		}
		myHeight[hash] = myTotal
	} else {
		return true
	}

	if checkpoint != nil && checkHeight != nil && hash.IsEqual(checkpoint) {
		*checkHeight = myTotal
	}

	return true
}
func myBlkHeightOfBlock(hsh [32]byte) uint64 {
	for i := 0; i < 16; i++ {
		hsh[i], hsh[31-i] = hsh[31-i], hsh[i]
	}
	myMutex.RLock()
	defer myMutex.RUnlock()
	return myHeight[hsh]
}
func myBlkHeight(hsh *chainhash.Hash) uint64 {
	myMutex.RLock()
	defer myMutex.RUnlock()
	return myHeight[*hsh]
}
func myStatus(fun func(tot uint64, hsh *chainhash.Hash)) uint64 {
	myMutex.RLock()

	var tot = myTotal
	var hsh = &myChain[myTotal]

	if fun != nil {
		fun(tot, hsh)
	}

	myMutex.RUnlock()
	return tot
}

func myLocator(burst byte, depth int, fun func(uint64, *chainhash.Hash)) error {
	myMutex.RLock()
	var i = int64(myTotal) - int64(depth)
	if i < 0 {
		i = 0
	}
	var chain = myChain
	myMutex.RUnlock()

	if int64(len(chain)) <= i {
		return errChainShortened
	}

	myLocatorHeight(i, burst, fun, chain)
	return nil
}

func myLocatorHeight(i int64, burst byte, fun func(uint64, *chainhash.Hash), chain []chainhash.Hash) {

	for j := i; j > i-int64(burst) && j >= 0; j-- {
		hash := &chain[uint64(j)]
		fun(uint64(j), hash)
	}
	var k int64 = 2
	for j := i - int64(burst) - 1; j >= 0; j -= k {
		k *= 2
		hash := &chain[uint64(j)]
		fun(uint64(j), hash)
	}

}

func myActivateChain() {
	myMutex.Lock()
	defer myMutex.Unlock()

	if myActiveTotalView != &myTotal {
		myActiveTotalView = &myTotal
		myActiveTotal = myTotal
		myActiveChain = &myChain
	}

}

func myDeactivateChain() {
	myReactivateChain(int64(ConfigRecycleHeight))
}

//	func myNewlyActivateChain() {
//		myMutex.Lock()
//		defer myMutex.Unlock()
//
//		if myActiveTotalView != &myActiveTotal {
//			myActiveTotal = myTotal
//			myActiveTotalView = &myActiveTotal
//
//		}
//		myTotal = 0
//		var genesis = myChain[0]
//		myChain = make([]chainhash.Hash, 0, 1000000)
//		// move genesis
//		myChain = append(myChain, genesis)
//
//		// reset bits and time
//		myResetBitsTime()
//	}

func myReactivateChain(height int64) {
	myMutex.Lock()
	defer myMutex.Unlock()

	// if negative, count from top of chain
	if height < 0 {
		height = int64(myTotal) + height
	}
	// if still negative, it was wrong, truncate it
	if height < 0 {
		height = 0
	}
	if myTotal < uint64(height) {
		height = int64(myTotal)
	}

	if myActiveTotalView != &myActiveTotal {
		myActiveTotal = myTotal
		myActiveTotalView = &myActiveTotal
		var myChainCopy = myChain
		myActiveChain = &myChainCopy
	}
	myTotal = uint64(height)
	var bakChain = myChain[:height+1]
	var initCap int64 = 1000000
	if initCap < height+1 {
		initCap = height + 1
	}
	myChain = make([]chainhash.Hash, height+1, initCap)
	copy(myChain, bakChain)

	// repair bits and time
	if height == 0 {

		if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
			myVerTimeNonceIfServer = myVerTimeNonceIfServer[:0]
			myMerkleRootsIfServer = myMerkleRootsIfServer[:0]
		}

		myResetBitsTime()
	} else {

		if myVerTimeNonceIfServer != nil && myMerkleRootsIfServer != nil {
			myVerTimeNonceIfServer = myVerTimeNonceIfServer[:height]
			myMerkleRootsIfServer = myMerkleRootsIfServer[:height]
		}

		myTruncateBitsTimes(uint64(height))
	}

}