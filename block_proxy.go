package main

import "sync"

var blockProxyMutex sync.RWMutex
var blockProxyMap = make(map[uint64]map[*FullNode]struct{})
var blockProxyBlockMap = make(map[*FullNode]map[uint64]struct{})

func blockProxyMapGetExactly(block [32]byte, fn *FullNode) bool {
	var blockhash = hash(string(block[:]))

	blockProxyMutex.RLock()
	defer blockProxyMutex.RUnlock()
	var map1 = blockProxyMap[blockhash]

	if map1 == nil {
		return false
	}
	_, ok := map1[fn]
	return ok
}
func blockProxyMapGet(block [32]byte) (out []*FullNode) {

	var blockhash = hash(string(block[:]))

	blockProxyMutex.RLock()
	defer blockProxyMutex.RUnlock()

	var map1 = blockProxyMap[blockhash]

	for k := range map1 {
		if k == nil {
			continue
		}
		out = append(out, k)
	}
	return
}
func blockProxyMapBlocksLen(block *FullNode) (out int) {
	blockProxyMutex.RLock()
	defer blockProxyMutex.RUnlock()

	return len(blockProxyBlockMap[block])
}

func blockProxyMapAdd(block [32]byte, recipient *FullNode) {

	var blockhash = hash(string(block[:]))

	blockProxyMutex.Lock()

	var map2 = blockProxyMap[blockhash]
	if map2 == nil {
		map2 = make(map[*FullNode]struct{})
	}

	map2[recipient] = struct{}{}
	blockProxyMap[blockhash] = map2

	var map3 = blockProxyBlockMap[recipient]
	if map3 == nil {
		map3 = make(map[uint64]struct{})
	}
	map3[blockhash] = struct{}{}
	blockProxyBlockMap[recipient] = map3

	blockProxyMutex.Unlock()
}

func blockProxyMapDeleteBlock(block [32]byte) {

	var blockhash = hash(string(block[:]))

	blockProxyMutex.Lock()

	var recipients = blockProxyMap[blockhash]
	delete(blockProxyMap, blockhash)
	for k := range recipients {
		delete(blockProxyBlockMap[k], blockhash)
	}

	blockProxyMutex.Unlock()
}

func blockProxyMapDeleteRecipient(recipient *FullNode) {

	blockProxyMutex.Lock()

	var blocks = blockProxyBlockMap[recipient]
	delete(blockProxyBlockMap, recipient)
	for k := range blocks {
		delete(blockProxyMap[k], recipient)
	}

	blockProxyMutex.Unlock()
}