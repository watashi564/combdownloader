# Comb Downloader

Comb downloader is a tool to sync a haircomb wallet with a bitcoin full node.

## Description

Setting up a bitcoin qt in RPC mode to serve blocks is not simple, it's done 
differently across the versions, and if not done correctly, may expose your BTC wallet
to your network. That's why there's this tool, that can sync your haircomb wallet
by connecting to any bitcoin full node, locally or remotely.

## Quick start

Run any recent version of bitcoin-qt, combfullui and this tool. No config needed.

## Versions

Tested with BTC 22.0 / BTC 23.0 / BTC 24.0 and combfullui 0.3.5.1 security patched.

### 0.0.15

Minor release. Tcp communication bug was traced to non millisecond accuracy
of timer, that caused the waitfornewblock calls to expire. Windows specific
bug fixed.

### 0.0.14

Minor release. Golang bumped to 1.20.2, which is supposed to fix the windows
specific tcp or webserver problem existing on 1.19.x.

### 0.0.13

Minor release. Regtest, reorg option, nested option.

### 0.0.12

Uploader mode. Make accelerators work properly, writeable accelerator files. Bloom mining.
My-btc-full-node can be specified multiple times. Recycle-headers is now -1 by default.
Full blocks proxying for BTCs in uploader mode. Bump user agent to Satoshi 24.0.1. Regtest mode.
Prevent file descriptor leak by setting the timeout.

### 0.0.11

Bump accelerator library.

### 0.0.10
Don't disconnect from my btc full node when bloom filters are forced.
Use bloomfilters even if not forced (if there are both files loaded and a bloom nodes).

### 0.0.9

Accelerators fix, still use at your caution.
Don't disconnect from localhost when bloom filters are forced.

### 0.0.8

Experimental accelerators, use at your caution.

### 0.0.7

Fix block header coming late for a newly mined block.

### 0.0.6

Tor don't run out of file descriptors.
Tor collect p2p nodes hostnames from the new addr message.
Price charting application.

### 0.0.5

Compatibility with Core 23 (protocol 70016 fix)

## Compatibility with BitcoinQT (Bitcoin core)

| BitcoinQT \ CombDL | 0.0.4 | 0.0.11 | 0.0.12 |
| -------------------|-------|--------|--------|
| /Satoshi:24.0.0/   | OK    | OK     | OK     |
| /Satoshi:23.0.0/   | FULL  | OK     | FULL   |
| /Satoshi:23.99.0/  | OK    | OK     | OK     |
| /Satoshi:22.99.0/  | OK    | OK     | OK     |
| /Satoshi:22.0.0/   | FULL  | OK     | OK     |
| /Satoshi:21.99.0/  | OK    | OK     | OK     |
| /Satoshi:0.8.6/    | OK    | OK     | OK     |
| /Satoshi:0.21.2/   | OK    | OK     | OK     |
| /Satoshi:0.21.1/   | OK    | OK     | OK     |
| /Satoshi:0.21.0/   | OK    | OK     | OK     |
| /Satoshi:0.20.1/   | OK    | OK     | OK     |
| /Satoshi:0.20.0/   | OK    | OK     | OK     |
| /Satoshi:0.19.1/   | OK    | OK     | OK     |
| /Satoshi:0.19.0.1/ | OK    | OK     | OK     |
| /Satoshi:0.18.1/   | OK    | OK     | OK     |
| /Satoshi:0.18.0/   | OK    | OK     | OK     |
| /Satoshi:0.17.1/   | OK    | OK     | OK     |
| /Satoshi:0.17.0.1/ | OK    | OK     | OK     |
| /Satoshi:0.16.3/   | OK    | OK     | OK     |
| /Satoshi:0.14.99/  | OK    | OK     | OK     |
| /Satoshi:0.12.1/   | OK    | OK     | OK     |

OK - downloading works
FULL - fully tested (including chain reorgs)

## Compatibility with other BTC clients

| other BTC client \ CombDL | 0.0.4 | 0.0.10 |
| --------------------------|-------|--------|
| /Satoshi:0.12.1(bitcore)/ | OK    | OK     |
| /Aurum:0.12.4/            | XXXXX | XXXXX  |
| /bcoin:2.1.2/             | OK    | OK     |

OK - downloading works
XXXXX - downloading not working

## Command line options

### accelerator=hash

Use this option to add a highest accelerator hash. That hash must be contained within the provided files, otherwise no accelerated download is made.
Watch out for **Accelerator hash could not be activated** errors when using this option.

### add-file-ro=filename.hax

Add accelerator file READ ONLY. Multiple accelerator files can be provided, they will all be used. If the files could be untrusted, please use the accelerator option to defend yourself.

### add-file-rw=filename.hax

Add accelerator file READ WRITEABLE. Multiple accelerator files can be provided, they will all be used. If the files could be untrusted, please use the accelerator option to defend yourself.

### force-bloom

Force bloom filters (accelerators) for all connections. You will disconnect from any bitcoin node that does not advertise bloom.
In 0.0.9, it is not disconnecting from my btc full node, if my btc full node flag is unset.
In 0.0.10, it is never disconnecting from my btc full node, and not downloading bloom blocks from it if it does not support it.

### usecase-accelerator

Enables extra data for the accelerator file generator application (transactions IDs in blocks). Not needed for accelerated download.

### usecase-pricecharts

Enables extra data for the price charts appliaction.

### add-node=127.0.0.1:8333

Adds a block puller node. Can be IPv4, domain, IPv6 or Tor. These nodes are only used to download blocks from, not headers.
Option can be used multiple times.

### addnode=127.0.0.1:8333

Same as add-node.

### checkpoint=hash

Specify a bitcoin block hash to use as checkpoint. This block must be reached, otherwise no download is made.

### extra-nodes=5

Specify how many extra block puller nodes, for instance 5 nodes, are discovered and used. They are discovered
from the bitcoin P2P network. Don't use this option if you need extra privacy or want to sync over the LAN only.

### comb-nodes=5

Specify how many extra comb (accelerator + bloom) nodes, for instance 5 nodes, are discovered and used. They are discovered
from the bitcoin P2P network. Don't use this option if you need extra privacy or want to sync over the LAN only.

### force-tor

Force tor for all connections.

### my-btc-full-node=127.0.0.1:8333

This specifies the Bitcoin Full node under the user's control whose blockchain is assumed to be the valid chain.
By default, the localhost full node is used. Because the downloader follows this blockchain, the haircomb core will
also follow this chain. Make sure you control and trust the node specified, otherwise the remote BTC node operator may serve you
some incorrect blockchain. Can be IPv4, domain, IPv6 or Tor.

### recycle-headers

Positive integer specifies height from which to start resyncing headers upon headers node
disconnect. Deeper reorgs are impossible.
Negative integer specifies how many blocks to remove from the top of chain upon headers node disconnect.
By default this option is -1 to reorg 1 block on disconnect. But longer reorgs while connected are handled normally.

### testnet

Sync with testnet. Changes genesis hash / default your btc node port / default RPC server port

### regtest=genesishash

Sync with regtest. Sets the genesis hash / default your btc node port / default RPC server port

### timeout

Pings/requests headers the header node every this many seconds. Does block garbage collection this many seconds.
Detect BTC node stall this frequently.

### tor

Specifies tor ip+port. Port is usually 9150 or 9151.

### miner-difficulty=number

Specifies miner-difficulty. Enter 0 to deactivate miner. The minimal difficulty (very cpu light) is 1.

### miner-functions=number

Specifies miner-functions. Enter 0 to deactivate miner. The recommended value is 7.

### uploader=ip:port

Starts a bitcoin server on ip:port. Enter 0.0.0.0:8333 to open port 8333 to the public. Used to seed headers, accelerators and blooms to help any nodes who need them.
This does serve (proxies) full blocks to bitcoin nodes, there a risk of large-scale traffic from your node.

### gossip=ip:port

Tells other nodes about ip:port, both when they want to know about nodes, or just when they connect. Gossip can be specified multiple times.

### nested

Allow nested mode, to connect combdownloader to another combdownloader on the same machine (disables RPC if possible)

### reorg=height

Do not sync above certain maximal height. Useful to reorg the client.

### v

Print version and exit.

### h

Print help and exit.