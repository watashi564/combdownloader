package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "math/big"

const nPowTargetTimespan = 14 * 24 * 60 * 60

var bigOne = big.NewInt(1)
var mainPowLimit = new(big.Int).Sub(new(big.Int).Lsh(bigOne, 224), bigOne)

func HashToBig(hash *chainhash.Hash) *big.Int {
	// A Hash is in little-endian, but the big package wants the bytes in
	// big-endian, so reverse them.
	buf := *hash
	blen := len(buf)
	for i := 0; i < blen/2; i++ {
		buf[i], buf[blen-1-i] = buf[blen-1-i], buf[i]
	}

	return new(big.Int).SetBytes(buf[:])
}

func CompactToBig(compact uint32) *big.Int {
	// Extract the mantissa, sign bit, and exponent.
	mantissa := compact & 0x007fffff
	isNegative := compact&0x00800000 != 0
	exponent := uint(compact >> 24)

	// Since the base for the exponent is 256, the exponent can be treated
	// as the number of bytes to represent the full 256-bit number.  So,
	// treat the exponent as the number of bytes and shift the mantissa
	// right or left accordingly.  This is equivalent to:
	// N = mantissa * 256^(exponent-3)
	var bn *big.Int
	if exponent <= 3 {
		mantissa >>= 8 * (3 - exponent)
		bn = big.NewInt(int64(mantissa))
	} else {
		bn = big.NewInt(int64(mantissa))
		bn.Lsh(bn, 8*(exponent-3))
	}

	// Make it negative if the sign bit is set.
	if isNegative {
		bn = bn.Neg(bn)
	}

	return bn
}

// BigToCompact converts a whole number N to a compact representation using
// an unsigned 32-bit number.  The compact representation only provides 23 bits
// of precision, so values larger than (2^23 - 1) only encode the most
// significant digits of the number.  See CompactToBig for details.
func BigToCompact(n *big.Int) uint32 {
	// No need to do any work if it's zero.
	if n.Sign() == 0 {
		return 0
	}

	// Since the base for the exponent is 256, the exponent can be treated
	// as the number of bytes.  So, shift the number right or left
	// accordingly.  This is equivalent to:
	// mantissa = mantissa / 256^(exponent-3)
	var mantissa uint32
	exponent := uint(len(n.Bytes()))
	if exponent <= 3 {
		mantissa = uint32(n.Bits()[0])
		mantissa <<= 8 * (3 - exponent)
	} else {
		// Use a copy to avoid modifying the caller's original number.
		tn := new(big.Int).Set(n)
		mantissa = uint32(tn.Rsh(tn, 8*(exponent-3)).Bits()[0])
	}

	// When the mantissa already has the sign bit set, the number is too
	// large to fit into the available 23-bits, so divide the number by 256
	// and increment the exponent accordingly.
	if mantissa&0x00800000 != 0 {
		mantissa >>= 8
		exponent++
	}

	// Pack the exponent, sign bit, and mantissa into an unsigned 32-bit
	// int and return it.
	compact := uint32(exponent<<24) | mantissa
	if n.Sign() < 0 {
		compact |= 0x00800000
	}
	return compact
}

func IsDifficultBlock(hash *chainhash.Hash, compact uint32) bool {
	actual := HashToBig(hash)
	target := CompactToBig(compact)
	return actual.Cmp(target) < 0
}

func CalculateNextWorkRequired(myLastTime2, myLastTime, myBits uint32) uint32 {

	var nActualTimespan = int64(myLastTime) - int64(myLastTime2)

	if nActualTimespan < nPowTargetTimespan/4 {
		nActualTimespan = nPowTargetTimespan / 4
	}
	if nActualTimespan > nPowTargetTimespan*4 {
		nActualTimespan = nPowTargetTimespan * 4
	}
	oldTarget := CompactToBig(myBits)
	newTarget := new(big.Int).Mul(oldTarget, big.NewInt(nActualTimespan))
	newTarget.Div(newTarget, big.NewInt(nPowTargetTimespan))

	// Limit new value to the proof of work limit.
	if newTarget.Cmp(mainPowLimit) > 0 {
		newTarget.Set(mainPowLimit)
	}
	newTargetBits := BigToCompact(newTarget)
	/*
		fmt.Printf("Old target %08x (%064x)", myBits, CompactToBig(myBits))
		fmt.Printf("New target %08x (%064x)", newTargetBits, CompactToBig(newTargetBits))
		fmt.Printf("Actual timespan %v, target timespan %v",
			(nActualTimespan),
			nPowTargetTimespan)
	*/

	return newTargetBits
}