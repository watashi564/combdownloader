package main

import "time"
import "sync"
import "bitbucket.org/watashi564/accelerator/libfile"
import "log"
import "io"
import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
)
import "os"

const SegwitGenesis = 481824
const Bip37MaxFilterSize = 36000

var acceleratorMutex sync.RWMutex
var acceleratorFile libfile.FileSet
var acceleratorHash [32]byte
var acceleratorHeight uint64
var acceleratorCounts []uint64
var acceleratorMinCount uint64
var acceleratorSizeTries = make(map[[32]byte][2]uint64)

func acceleratorCountsAdd(counts *[]uint64, minCount *uint64, height, accCount uint64) {

	if *counts == nil && *minCount == 0 {
		*minCount = height
		*counts = append(*counts, accCount)
	} else if *minCount > height {

		*counts = append(*counts, make([]uint64, *minCount-height)...)
		copy((*counts)[*minCount-height:], *counts)
		*minCount = height
		(*counts)[0] = accCount
	} else if *minCount+uint64(len(*counts)) < height {
		*counts = append(*counts, make([]uint64, height-*minCount+uint64(len(*counts)))...)
		*counts = append(*counts, accCount)
	} else if uint64(len(*counts))+*minCount == height {
		*counts = append(*counts, accCount)
	} else {
		(*counts)[height-*minCount] = accCount
	}
	if (*counts)[height-*minCount] != accCount {
		panic("???")
	}
}

func acceleratorUnhex(str string) *[32]byte {
	if len(str) == 0 {
		return nil
	}

	if len(str) != 64 {
		return &[32]byte{}
	}
	var buf [32]byte
	n, err := hex.Decode(buf[:], []byte(str))
	if err != nil {
		return &[32]byte{}
	}
	if n != 32 {
		return &[32]byte{}
	}
	return &buf
}

func acceleratorBloomExist(blockHash [32]byte, count, newsize uint64) (exist bool, smaller bool) {
	acceleratorMutex.RLock()
	file := acceleratorFile
	acceleratorMutex.RUnlock()
	if file == nil {
		return false, true // causes no check, don't use
	}
	var bloom = file.Get(blockHash, count, 0, Bip37MaxFilterSize)
	if bloom == nil {
		return false, false // causes no check, use
	}
	if bloom.PayloadSize() <= newsize {
		return true, false // causes no check, dont use
	}
	return true, true // causes check, use
}

// TODO: optimize
func acceleratorsDownloadBlooms(accels []Accelerator, file libfile.FileSet) {
	for bugs := 1; bugs > 0; {
		bugs = 0
		for _, acc := range accels {
			var blockHash = acc.BlockHash
			if acc.Count == 0 {
				continue
			}

			var present = file.Get(blockHash, acc.Count, 0, Bip37MaxFilterSize)
			if present == nil {

				for !HaveBloomOnlyNode() {
					time.Sleep(100 * time.Millisecond)
				}

				var bloom = PullBloom(blockHash, acc.Count)
				if bloom == nil {
					bugs++
					continue
				}
				err := file.Add(&libfile.BloomFilter{
					BlockHash:  blockHash,
					P2wshCount: acc.Count,
					Payload:    bloom,
				})
				if err != nil && err != io.EOF {
					println(err.Error())
				}
				var serialized = acc.Serialize()
				var sha = acc.Sha()
				err = file.Add(&libfile.BloomFilter{
					BlockHash:  sha,
					P2wshCount: ^uint64(0),
					Payload:    serialized[:],
				})
				if err != nil && err != io.EOF {
					println(err.Error())
				}
			}
		}
	}
}
func acceleratorsDownload(hash [32]byte, file libfile.FileSet, write bool) {
	var topHash = hash
	var cnt uint64
	var allAccelerators []Accelerator
	var counts []uint64
	var minCount uint64
	for hash != [32]byte{} {
		for !HaveBloomOnlyNode() {
			time.Sleep(100 * time.Millisecond)
		}

		var accs = PullAccelerators(hash, [32]byte{})
		if len(accs) == 0 {
			continue
		}
		var wantedhash = accs[len(accs)-1].Sha()
		if wantedhash != hash {
			return
		}
		//fmt.Printf("%X=%X\n", hash, wantedhash)
		for i, acc := range accs {
			if i == 0 {
				hash = acc.Hash
			}
			cnt++
			//fmt.Printf("%X %X %d\n", acc.Hash, acc.BlockHash, acc.Count)
			var height = myBlkHeightOfBlock(acc.BlockHash)
			if height == 0 {
				return
			}
			acceleratorCountsAdd(&counts, &minCount, height, acc.Count)

		}
		for i := 0; i < len(accs)/2; i++ {
			accs[i], accs[len(accs)-1-i] = accs[len(accs)-1-i], accs[i]
		}
		allAccelerators = append(allAccelerators, accs...)
	}
	acceleratorMutex.Lock()
	acceleratorFile = file
	acceleratorHash = topHash
	acceleratorHeight = SegwitGenesis + cnt
	acceleratorCounts = counts
	acceleratorMinCount = minCount
	acceleratorMutex.Unlock()
	log.Printf("ACTIVATED %d %X\n", SegwitGenesis+cnt-1, topHash)

	if !write {
		return
	}
	go miner()

	if len(allAccelerators) > 0 {
		allAccelerators = append(allAccelerators, allAccelerators[0])
	}

	for i := 0; i < len(allAccelerators)/2; i++ {
		allAccelerators[i], allAccelerators[len(allAccelerators)-1-i] = allAccelerators[len(allAccelerators)-1-i], allAccelerators[i]
	}

	acceleratorsDownloadBlooms(allAccelerators, file)
}

func acceleratorOpen(filenamesRW, filenamesReadonly []string, hash *[32]byte) {
	acceleratorMutex.RLock()
	oldFile := acceleratorFile
	acceleratorMutex.RUnlock()
	if oldFile != nil {
		return
	}
	acceleratorMutex.Lock()
	oldFile = acceleratorFile
	acceleratorFile = libfile.FileSet{}
	acceleratorMutex.Unlock()
	if oldFile != nil {
		return
	}

	var hashBuffer = make(map[[32]byte]uint64)
	var p2WSHBuffer = make(map[uint64]uint64)

	var maxHeight uint64
	var maxHash [32]byte

	if len(filenamesReadonly)+len(filenamesRW) == 0 {
		filenamesRW = []string{os.TempDir() + string(os.PathSeparator) + "accelerators.hax"}
	}

	file, err := libfile.OpenManyEachMaxPayload(filenamesRW, filenamesReadonly, 72, func(f *libfile.BloomFilter) {
		if f.P2wshCount == ^uint64(0) && f.PayloadSize() == 72 {
			// here f.BlockHash is actually the accelerator hash
			if sha256.Sum256(f.Payload) == f.BlockHash {

				var blockhash [32]byte

				copy(blockhash[0:32], f.Payload[32:64])

				var height = myBlkHeightOfBlock(blockhash)
				var count = binary.BigEndian.Uint64(f.Payload[64:72])
				if height > 0 {
					if count > p2WSHBuffer[height] {
						hashBuffer[f.BlockHash] = height + 1
						p2WSHBuffer[height] = count
					}
					//fmt.Printf("%d %d %X %X\n", height, count, f.BlockHash, f.Payload)
				}
				if height > maxHeight {
					maxHeight = height
					maxHash = f.BlockHash
				}
			}
		}
	})
	if err != nil {
		println(err.Error())
	}
	var counts []uint64
	var minCount uint64
	if hash != nil {
		if acceleratorActivateHash(&file, *hash, &counts, &minCount) {
			acceleratorMutex.Lock()
			acceleratorFile = file
			acceleratorHash = *hash
			acceleratorHeight = hashBuffer[*hash]
			acceleratorCounts = counts
			acceleratorMinCount = minCount
			acceleratorMutex.Unlock()
			log.Printf("ACTIVATED %d %X\n", hashBuffer[*hash]-1, *hash)
			go miner()
		} else {
			acceleratorsDownload(*hash, file, len(filenamesRW) > 0)
		}
	} else {
		if acceleratorActivateHash(&file, maxHash, &counts, &minCount) {
			acceleratorMutex.Lock()
			acceleratorFile = file
			acceleratorHash = maxHash
			acceleratorHeight = maxHeight + 1
			acceleratorCounts = counts
			acceleratorMinCount = minCount
			acceleratorMutex.Unlock()
			log.Printf("ACTIVATED %d %X\n", maxHeight, maxHash)
			go miner()
		}
	}
}

func acceleratorActivateHash(file *libfile.FileSet, hash [32]byte, counts *[]uint64, minCount *uint64) bool {

	var data = file.Get(hash, ^uint64(0), 72, 72)
	if data == nil {
		log.Println("Accelerator hash could not be activated: Data not found")
		return false
	}
	if sha256.Sum256(data.Payload) != data.BlockHash {
		log.Println("Accelerator hash could not be activated: Data don't hash")
		return false
	}
	var blockhash [32]byte

	copy(blockhash[0:32], data.Payload[32:64])

	var height = myBlkHeightOfBlock(blockhash)
	if height == 0 {
		log.Println("Accelerator hash could not be activated: Height not found")
		return false
	}

	for height >= SegwitGenesis {

		data = file.Get(hash, ^uint64(0), 72, 72)
		if data == nil {
			log.Printf("Accelerator hash could not be activated (height %d): Data not found\n", height)
			return false
		}
		if sha256.Sum256(data.Payload) != data.BlockHash {
			log.Printf("Accelerator hash could not be activated (height %d): Data don't hash\n", height)
			return false
		}
		var blockhash [32]byte
		copy(hash[0:32], data.Payload[0:32])
		copy(blockhash[0:32], data.Payload[32:64])
		var count = binary.BigEndian.Uint64(data.Payload[64:72])
		if myBlkHeightOfBlock(blockhash) != height {
			log.Printf("Accelerator hash could not be activated (height %d): Height not smaller by one\n", height)
			log.Printf("Accelerator below is %X\n", hash)
			return false
		}

		if count > 0 {
			var bloom = file.Get(blockhash, count, 0, Bip37MaxFilterSize)
			if bloom == nil {
				log.Printf("Accelerator hash could not be activated (height %d): Bloom not found\n", height)
				log.Printf("Accelerator block is %x, count %d\n", blockhash, count)
				log.Printf("Accelerator below is %X\n", hash)
				return false
			}
		}
		if *counts == nil && *minCount == 0 {
			*minCount = height
		}
		acceleratorCountsAdd(counts, minCount, height, count)
		height--
	}
	return true
}

func acceleratorAttach(a Accelerator) error {
	acceleratorMutex.RLock()
	file := acceleratorFile
	acceleratorMutex.RUnlock()

	if file == nil {
		return nil
	}

	var buf = a.Sha()
	var load = a.Serialize()
	err := file.Add(&libfile.BloomFilter{
		BlockHash:  buf,
		P2wshCount: ^uint64(0),
		Payload:    load[:],
	})
	if err != nil {
		return err
	}

	acceleratorMutex.Lock()
	if acceleratorHash == a.Hash {
		acceleratorHash = buf
		acceleratorCountsAdd(&acceleratorCounts, &acceleratorMinCount, acceleratorHeight, a.Count)
		acceleratorHeight++
	}
	acceleratorMutex.Unlock()
	return nil
}

func acceleratorTop() ([32]byte, uint64, libfile.FileSet) {
	acceleratorMutex.RLock()
	var hash = acceleratorHash
	var topHeight = acceleratorHeight
	var file = acceleratorFile
	acceleratorMutex.RUnlock()
	return hash, topHeight, file
}

func acceleratorCount(height uint64) uint64 {
	if height == 0 {
		return ^uint64(0)
	}
	acceleratorMutex.RLock()
	var hash = acceleratorHash
	var topHeight = acceleratorHeight
	var count, okCount = uint64(0), (height >= acceleratorMinCount) && (uint64(len(acceleratorCounts))+acceleratorMinCount > height)
	if okCount {
		count = acceleratorCounts[height-acceleratorMinCount]
	}
	var file = acceleratorFile
	acceleratorMutex.RUnlock()

	if height >= topHeight {
		return ^uint64(0)
	}

	if !okCount && height > 1 {
		return ^uint64(0)
	}

	var data = file.Get(hash, ^uint64(0), 72, 72)
	if data == nil {
		//println("data nil")
		return ^uint64(0)
	}
	var readedHash [32]byte
	copy(readedHash[0:32], data.Payload[32:64])

	var topBlockHashCH, ok = myGetHash(topHeight - 1)
	if !ok {
		return ^uint64(0)
	}
	var topBlockHash = [32]byte(topBlockHashCH)

	for i := 0; i < 16; i++ {
		topBlockHash[i], topBlockHash[31-i] = topBlockHash[31-i], topBlockHash[i]
	}
	// acceleratorHash ceased to contain the headers chain
	// need to trigger reorg
	if readedHash != topBlockHash {
		//fmt.Printf("%x %x\n", readedHash, topBlockHash)
		return ^uint64(0)
	}

	return count
}
func acceleratorHeader(hash [32]byte) ([32]byte, [32]byte, uint64, bool) {
	acceleratorMutex.RLock()
	var file = acceleratorFile
	acceleratorMutex.RUnlock()

	var data = file.Get(hash, ^uint64(0), 72, 72)
	if data == nil || len(data.Payload) != 72 {
		return [32]byte{}, [32]byte{}, 0, false
	}
	if hash != sha256.Sum256(data.Payload) {
		return [32]byte{}, [32]byte{}, 0, false
	}
	var accbuf, blockbuf [32]byte
	copy(accbuf[0:32], data.Payload[0:32])
	copy(blockbuf[0:32], data.Payload[32:64])
	var count = binary.BigEndian.Uint64(data.Payload[64:72])
	return accbuf, blockbuf, count, true
}
func acceleratorGetCnt(blk [32]byte, count uint64) []byte {
	acceleratorMutex.RLock()
	var file = acceleratorFile
	acceleratorMutex.RUnlock()

	acc := file.GetSmallest(blk, count, 0, Bip37MaxFilterSize)
	if acc == nil {
		return nil
	}

	return acc.Payload
}
func acceleratorGet(blk [32]byte, height uint64) []byte {

	if height == 0 {
		return nil
	}

	var count = acceleratorCount(height)
	if count == ^uint64(0) {
		//println("count wrong")
		return nil
	}

	acceleratorMutex.RLock()
	var read = acceleratorSizeTries[blk]
	var file = acceleratorFile
	acceleratorMutex.RUnlock()

	if count == 0 {
		acceleratorMutex.Lock()
		acceleratorSizeTries[blk] = [2]uint64{uint64(0), read[1] + 1}
		acceleratorMutex.Unlock()
		//println("builtin")
		// returning the builtin give me no transactions accelerator for empty blocks
		return []byte{0, 0, 0, 0, 1, 0, 0, 0, 0}
	}

	if file == nil {
		//println("file nil")
		return nil
	}

	acc := file.GetSmallest(blk, count, read[0]+1, Bip37MaxFilterSize)
	if acc == nil {
		//println("acc nil")
		return nil
	}

	acceleratorMutex.Lock()
	var exceeded = (ConfigAccCap > 0) && (len(acceleratorSizeTries) > ConfigAccCap)
	if exceeded {
		for k := range acceleratorSizeTries {
			delete(acceleratorSizeTries, k)
			break
		}
	}
	acceleratorSizeTries[blk] = [2]uint64{uint64(len(acc.Payload)), read[1] + 1}
	acceleratorMutex.Unlock()

	//println("ok")
	return acc.Payload
}

func acceleratorDelete(blk [32]byte) {
	acceleratorMutex.Lock()
	delete(acceleratorSizeTries, blk)
	acceleratorMutex.Unlock()
}