package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "github.com/btcsuite/btcd/wire"
import "net"
import "fmt"
import "sync"
import "time"
import "crypto/sha256"
import "encoding/binary"

var FullNodesMutex sync.RWMutex
var FullNodes = make(map[*FullNode]byte)

const FullNodePingable = 1
const FullNodeDownloadable = 2
const FullNodeDownloadableAndBloomable = 4
const FullNodeAcceleratorBloom = 8
const FullNodeUploader = 16
const FullNodeSegDownloadable = 32

var FullBlocksMutex sync.RWMutex
var FullBlocks = make(map[[32]byte]*Block)

var FullAcceleratorsMutex sync.RWMutex
var FullAccelerators = make(map[[32]byte][]Accelerator)

var FullBloomMutex sync.RWMutex
var FullBloom = make(map[[40]byte][]byte)

func NotifyBlock(hash [32]byte, blk *Block) {
	FullBlocksMutex.Lock()
	defer FullBlocksMutex.Unlock()
	FullBlocks[hash] = blk
}

func NotifyAccelerators(hash [32]byte, acc []Accelerator) {
	FullAcceleratorsMutex.Lock()
	defer FullAcceleratorsMutex.Unlock()
	FullAccelerators[hash] = acc
}

func NotifyBloom(hashcount []byte, bloom []byte) {
	var buf [40]byte
	copy(buf[0:40], hashcount)

	FullBloomMutex.Lock()
	defer FullBloomMutex.Unlock()
	FullBloom[buf] = bloom
}

func GetFullNode(bits byte) *FullNode {
	FullNodesMutex.RLock()
	defer FullNodesMutex.RUnlock()

	for k, v := range FullNodes {
		if (v & bits) != 0 {
			return k
		}
	}
	return nil
}
func GetFullNodes(bits byte) (out map[*FullNode]struct{}) {

	out = make(map[*FullNode]struct{})

	FullNodesMutex.RLock()
	defer FullNodesMutex.RUnlock()

	for k, v := range FullNodes {
		if (v & bits) != 0 {
			out[k] = struct{}{}
		}
	}
	return nil
}

func InvFullNodes(hdr [32]byte, bits byte) {

	var nodes []*FullNode

	FullNodesMutex.RLock()
	for k, v := range FullNodes {
		if v&bits != 0 {
			nodes = append(nodes, k)
		}
	}
	FullNodesMutex.RUnlock()
	for _, k := range nodes {
		err := k.Invite(hdr)
		if err != nil {
			println(err.Error())
		}
	}
}

func PutFullNode(fn *FullNode, bits byte) {
	FullNodesMutex.Lock()
	defer FullNodesMutex.Unlock()

	FullNodes[fn] = bits
}

func DeleteFullNode(fn *FullNode) {
	FullNodesMutex.Lock()
	defer FullNodesMutex.Unlock()

	delete(FullNodes, fn)
}

func PingNodes() (err error) {
	var nodes = GetFullNodes(FullNodePingable)
	for node := range nodes {
		err2 := node.Ping()
		if err == nil {
			err = err2
		} else if err2 != nil {
			err = fmt.Errorf("ping: %e, %e", err2, err)
		}
	}
	return err
}
func GetHeaderNode(hdr [32]byte) error {
	var node = GetFullNode(FullNodePingable)
	if node == nil {
		return nil
	}

	if !node.HeaderSyncOver() {
		return nil
	}

	return node.GetHeaders(hdr)
}

func GetHeadersNode() error {
	return GetHeaderNode([32]byte{})
}

func PullAcceleratedBlockOnline(hash [32]byte, height uint64) *Block {
	var f = acceleratorGet(hash, height)
	if f != nil {
		//println("got local accelerator")
		return PullAcceleratedBlock(hash, f)
	}
	if HaveBloomNode() {
		//println("fetching count by height", height)
		var c = acceleratorCount(height)
		if c == 0 {
			//println("returning the builtin give me no transactions accelerator for empty blocks")
			// returning the builtin give me no transactions accelerator for empty blocks
			f = []byte{0, 0, 0, 0, 1, 0, 0, 0, 0}
			return PullAcceleratedBlock(hash, f)
		}
		if c == ^uint64(0) {
			//println("no accelerator, pulling slowly")
			return PullBlock(hash)
		}

		//println("pulling bloom from special node")
		f = PullBloom(hash, c)
		if f != nil {
			//println("got bloom pulling block")
			return PullAcceleratedBlock(hash, f)
		}
	}
	//println("no bloom, pulling slowly")
	return PullBlock(hash)
}
func HaveBloomNode() bool {
	return nil != GetFullNode(FullNodeDownloadableAndBloomable) &&
		nil != GetFullNode(FullNodeAcceleratorBloom)
}
func HaveBloomOnlyNode() bool {
	return nil != GetFullNode(FullNodeAcceleratorBloom)
}
func PullAcceleratedBlock(hash [32]byte, filter []byte) *Block {

	initialBlk := RetrieveBlock(hash)
	if initialBlk != nil {
		return initialBlk
	}

	var known = make(map[*FullNode]struct{})

	for i := 0; i < 6; i++ {
		var node = GetFullNode(FullNodeDownloadableAndBloomable)

		if node == nil {
			continue
		}

		if _, ok := known[node]; ok {
			continue
		}

		known[node] = struct{}{}

		blk := node.DownloadFilteredBlock(hash, filter, 2000)
		if blk == nil {
			continue
		} else {
			return blk
		}

	}
	for n := range known {
		if !n.IsForever() {
			n.Disconnect()
		}
	}
	return nil
}
func PullBlock(hash [32]byte) *Block {

	initialBlk := RetrieveBlock(hash)
	if initialBlk != nil {
		return initialBlk
	}

	var known = make(map[*FullNode]struct{})

	for i := 0; i < 6; i++ {
		var node = GetFullNode(FullNodeDownloadable)

		if node == nil {
			continue
		}

		if _, ok := known[node]; ok {
			continue
		}

		known[node] = struct{}{}

		blk := node.DownloadBlock(hash, 2000)
		if blk == nil {
			continue
		} else {
			return blk
		}

	}
	for n := range known {
		if !n.IsForever() {
			n.Disconnect()
		}
	}
	return nil
}
func (a *Accelerator) Serialize() (out [72]byte) {
	copy(out[0:32], a.Hash[:])
	copy(out[32:64], a.BlockHash[:])
	binary.BigEndian.PutUint64(out[64:72], a.Count)
	return
}
func (a *Accelerator) Sha() (out [32]byte) {
	hash := sha256.New()
	var serial = a.Serialize()
	hash.Write(serial[:])
	copy(out[:], hash.Sum(nil))
	return
}

type Accelerator struct {
	Hash      [32]byte
	BlockHash [32]byte
	Count     uint64
}

func PullBloom(hash [32]byte, count uint64) []byte {
	initialBloom := RetrieveBloom(hash, count)
	if initialBloom != nil {
		return initialBloom
	}

	var known = make(map[*FullNode]struct{})

	for i := 0; i < 6; i++ {
		var node = GetFullNode(FullNodeAcceleratorBloom)

		if node == nil {
			continue
		}

		if _, ok := known[node]; ok {
			continue
		}

		known[node] = struct{}{}

		blk := node.DownloadBloom(hash, count, 2000)
		if blk == nil {
			continue
		} else {
			return blk
		}

	}
	for n := range known {
		if !n.IsForever() {
			n.Disconnect()
		}
	}
	return nil
}

func PullAccelerators(hash, endhash [32]byte) []Accelerator {
	initialBlk := RetrieveAccelerators(hash)
	if initialBlk != nil {
		return initialBlk
	}

	var known = make(map[*FullNode]struct{})

	for i := 0; i < 6; i++ {
		var node = GetFullNode(FullNodeAcceleratorBloom)

		if node == nil {
			continue
		}

		if _, ok := known[node]; ok {
			continue
		}

		known[node] = struct{}{}

		blk := node.DownloadAccelerators(hash, endhash, 2000)
		if blk == nil {
			continue
		} else {
			return blk
		}

	}
	for n := range known {
		if !n.IsForever() {
			n.Disconnect()
		}
	}
	return nil
}

func GcBlocks() {

	FullAcceleratorsMutex.Lock()
	FullAccelerators = make(map[[32]byte][]Accelerator)
	FullAcceleratorsMutex.Unlock()

	FullBloomMutex.Lock()
	FullBloom = make(map[[40]byte][]byte)
	FullBloomMutex.Unlock()

	ttime := time.Now().Add(time.Minute * -1)
	var garbageCollect = make(map[[32]byte]struct{})

	FullBlocksMutex.RLock()

	for k, v := range FullBlocks {
		if ttime.After(v.pulled) {
			garbageCollect[k] = struct{}{}
		}
	}

	FullBlocksMutex.RUnlock()

	if len(garbageCollect) > 0 {

		FullBlocksMutex.Lock()

		for k := range garbageCollect {
			delete(FullBlocks, k)
		}

		//println("gc", len(garbageCollect), len(FullBlocks))
		FullBlocksMutex.Unlock()

		for k := range garbageCollect {
			acceleratorDelete(k)
		}

	}
}

func RetrieveBlock(hash [32]byte) *Block {
	FullBlocksMutex.RLock()
	defer FullBlocksMutex.RUnlock()

	if blk := FullBlocks[hash]; blk != nil {
		return blk
	}
	return nil
}
func RetrieveAccelerators(hash [32]byte) []Accelerator {
	FullAcceleratorsMutex.RLock()
	defer FullAcceleratorsMutex.RUnlock()

	if blk := FullAccelerators[hash]; blk != nil {
		return blk
	}
	return nil
}
func RetrieveBloom(hash [32]byte, count uint64) []byte {

	var buf [40]byte
	copy(buf[0:32], hash[:])
	binary.BigEndian.PutUint64(buf[32:40], count)

	FullBloomMutex.RLock()
	defer FullBloomMutex.RUnlock()

	if blk := FullBloom[buf]; blk != nil {
		return blk
	}
	return nil
}

// downloads filtered block, returns it, or nil
func (fn *FullNode) DownloadFilteredBlock(hash [32]byte, filter []byte, timeoutMs int) *Block {

	hsh, err := chainhash.NewHashFromStr(fmt.Sprintf("%x", hash))
	if err != nil {
		return nil
	}
	err = downloader_download_filtered_block(fn.conn, fn.pver, fn.btcnet, hsh, filter)
	if err != nil {
		return nil
	}
	for i := 0; i < timeoutMs; i++ {
		FullBlocksMutex.Lock()
		if blk := FullBlocks[hash]; blk != nil {
			delete(FullBlocks, hash)
			FullBlocksMutex.Unlock()
			return blk

		}

		FullBlocksMutex.Unlock()

		time.Sleep(time.Millisecond)
	}

	return nil
}

// downloads block, returns it, or nil
func (fn *FullNode) DownloadBlock(hash [32]byte, timeoutMs int) *Block {

	hsh, err := chainhash.NewHashFromStr(fmt.Sprintf("%x", hash))
	if err != nil {
		return nil
	}
	err = downloader_download_block(fn.conn, fn.pver, fn.btcnet, hsh)
	if err != nil {
		return nil
	}
	for i := 0; i < timeoutMs; i++ {
		FullBlocksMutex.Lock()
		if blk := FullBlocks[hash]; blk != nil {
			delete(FullBlocks, hash)
			FullBlocksMutex.Unlock()
			return blk

		}

		FullBlocksMutex.Unlock()

		time.Sleep(time.Millisecond)
	}

	return nil
}

// downloads accelerators, returns it, or nil
func (fn *FullNode) DownloadAccelerators(hash, endhash [32]byte, timeoutMs int) []Accelerator {
	err := downloader_download_accelerators(fn.conn, fn.pver, fn.btcnet, chainhash.Hash(hash), chainhash.Hash(endhash))
	if err != nil {
		return nil
	}
	for i := 0; i < timeoutMs; i++ {
		FullAcceleratorsMutex.Lock()
		if acc := FullAccelerators[hash]; acc != nil {
			delete(FullAccelerators, hash)
			FullAcceleratorsMutex.Unlock()
			return acc

		}

		FullAcceleratorsMutex.Unlock()

		time.Sleep(time.Millisecond)
	}

	return nil
}

// downloads bloom, returns it, or nil
func (fn *FullNode) DownloadBloom(hash [32]byte, count uint64, timeoutMs int) []byte {
	err := downloader_download_bloom(fn.conn, fn.pver, fn.btcnet, hash, count)
	if err != nil {
		return nil
	}
	for i := 0; i < timeoutMs; i++ {

		var buf [40]byte
		copy(buf[0:32], hash[:])
		binary.BigEndian.PutUint64(buf[32:40], count)

		FullBloomMutex.Lock()
		if bloom := FullBloom[buf]; bloom != nil {
			delete(FullBloom, buf)
			FullBloomMutex.Unlock()
			return bloom

		}

		FullBloomMutex.Unlock()

		time.Sleep(time.Millisecond)
	}

	return nil
}

var WiringMutex sync.Mutex

// run filtered downloader for hash
func downloader_download_filtered_block(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hash *chainhash.Hash, filter []byte) error {

	hashFuncs := binary.BigEndian.Uint32(filter[len(filter)-8 : len(filter)-4])
	tweak := binary.BigEndian.Uint32(filter[len(filter)-4:])

	newFilter := wire.NewMsgFilterLoad(filter[0:len(filter)-8], hashFuncs, tweak, wire.BloomUpdateNone)

	return downloader_download_filtered_block2(conn, pver, btcnet, hash, newFilter)
}

// run filtered downloader for hash
func downloader_download_filtered_block2(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hash *chainhash.Hash, filter *wire.MsgFilterLoad) error {

	clearFilter := wire.NewMsgFilterClear()

	newFilter := filter

	newData := wire.NewMsgGetData()

	if err := newData.AddInvVect(wire.NewInvVect(wire.InvTypeFilteredBlock, hash)); err != nil {
		return err
	}

	WiringMutex.Lock()
	defer WiringMutex.Unlock()
	if err := wire.WriteMessage(*conn, clearFilter, pver, btcnet); err != nil {
		return err
	}
	if err := wire.WriteMessage(*conn, newFilter, pver, btcnet); err != nil {
		return err
	}
	if err := wire.WriteMessage(*conn, newData, pver, btcnet); err != nil {
		return err
	}
	return nil
}

// run downloader for hash
func downloader_download_block(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hash *chainhash.Hash) error {

	newData := wire.NewMsgGetData()

	if err := newData.AddInvVect(wire.NewInvVect(wire.InvTypeBlock, hash)); err != nil {
		return err
	}

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newData, pver, btcnet); err != nil {
		return err
	}
	return nil
}

// run downloader for accelerator hash
func downloader_download_accelerators(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hash, endhash chainhash.Hash) error {

	var null chainhash.Hash

	newData := wire.NewMsgGetHeaders()

	newData.BlockLocatorHashes = append(newData.BlockLocatorHashes, &hash)
	if endhash != null {
		newData.BlockLocatorHashes = append(newData.BlockLocatorHashes, &endhash)
	}
	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newData, pver, btcnet); err != nil {
		return err
	}
	return nil
}

// run downloader for bloom
func downloader_download_bloom(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hash [32]byte, count uint64) error {

	newData := wire.NewMsgFilterLoad(hash[:], uint32(count>>32), uint32(count), 0)

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newData, pver, btcnet); err != nil {
		return err
	}
	return nil
}

func (fn *FullNode) Ping() error {

	fn.hadMut.Lock()
	var origKick = fn.kick
	fn.kick = true
	fn.hadMut.Unlock()

	if origKick {
		(*fn.conn).Close()
		return errPingNoResponse
	}

	return ping_node(fn.conn, fn.pver, fn.btcnet, func(nonce uint64) {
		fn.hadMut.Lock()
		fn.kick = true
		fn.nonce = nonce
		fn.hadMut.Unlock()
	})
}

func ping_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, fun func(uint64)) error {

	nonce, err := wire.RandomUint64()
	if err != nil {
		return err
	}

	fun(nonce)

	newData := wire.NewMsgPing(nonce)

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newData, pver, btcnet); err != nil {
		return err
	}
	return nil
}

func (fn *FullNode) GetHeaders(hdr [32]byte) error {

	fn.hadMut.Lock()
	fn.newHdr = hdr
	fn.hadMut.Unlock()

	return get_headers_node(fn.conn, fn.pver, fn.btcnet, 0)
}
func (fn *FullNode) Invite(hdr chainhash.Hash) error {

	return invite_headers_node(fn.conn, fn.pver, fn.btcnet, hdr)
}

func invite_headers_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, hdr chainhash.Hash) error {

	newHdrs := wire.NewMsgInvSizeHint(2)
	err := newHdrs.AddInvVect(wire.NewInvVect(wire.InvTypeWitnessBlock, &hdr))
	if err != nil {
		return err
	}
	err = newHdrs.AddInvVect(wire.NewInvVect(wire.InvTypeBlock, &hdr))
	if err != nil {
		return err
	}

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newHdrs, pver, btcnet); err != nil {
		return err
	}

	return nil
}
func get_headers_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, depth int) error {

	newHdrs := wire.NewMsgGetHeaders()

	newHdrs.ProtocolVersion = pver

	err := myLocator(10, depth, func(id uint64, hash *chainhash.Hash) {
		if err := newHdrs.AddBlockLocatorHash(hash); err != nil {
			println(err.Error())
		}
	})
	if err != nil {
		return err
	}

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(*conn, newHdrs, pver, btcnet); err != nil {
		return err
	}

	return nil
}

func AddrFullNodes() {

	var nodes []*FullNode

	FullNodesMutex.RLock()
	for k := range FullNodes {
		nodes = append(nodes, k)
	}
	FullNodesMutex.RUnlock()
	for _, k := range nodes {
		err := k.GetAddr()
		if err != nil {
			println(err.Error())
		}
	}
}
func (fn *FullNode) GetAddr() error {

	return get_addr_node(fn.conn, fn.pver, fn.btcnet)
}
func get_addr_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet) error {
	var minBullshitMessagesAcceptedVersion = pver >= wire.AddrV2Version

	if minBullshitMessagesAcceptedVersion {
		var newAddr = wire.NewMsgSendAddrV2()

		WiringMutex.Lock()
		defer WiringMutex.Unlock()

		if err := wire.WriteMessage(*conn, newAddr, pver, btcnet); err != nil {
			return err
		}

	} else {

		var newAddr = wire.NewMsgGetAddr()

		WiringMutex.Lock()
		defer WiringMutex.Unlock()

		if err := wire.WriteMessage(*conn, newAddr, pver, btcnet); err != nil {
			return err
		}

	}

	return nil
}

func deliver_block_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet, msg wire.Message) error {
	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if _, err := wire.WriteMessageWithEncodingN(*conn, msg, pver, btcnet, wire.WitnessEncoding); err != nil {
		return err
	}

	return nil
}