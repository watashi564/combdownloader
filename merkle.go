package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"

func merkleTree(merkle []chainhash.Hash) chainhash.Hash {

	if len(merkle) == 0 {
		return chainhash.Hash{}
	}

	if len(merkle) == 1 {
		return merkle[0]
	}

	for len(merkle) > 1 {

		if len(merkle)%2 != 0 {
			merkle = append(merkle, merkle[len(merkle)-1])
		}

		var newMerkle []chainhash.Hash

		for i := 0; i < len(merkle)/2; i++ {

			var buf [64]byte

			copy(buf[0:32], merkle[2*i][0:])
			copy(buf[32:64], merkle[2*i+1][0:])

			newMerkle = append(newMerkle, chainhash.DoubleHashH(buf[0:64]))

		}

		merkle = newMerkle

	}
	return merkle[0]
}

type merkleTreeNode struct {
	leafNum uint
	childs  [2]*merkleTreeNode
}

func merkleKnownShape(numtx uint) *merkleTreeNode {

	if numtx == 0 {
		return nil
	}

	if numtx == 1 {
		return &merkleTreeNode{leafNum: 1}
	}
	var prevlayer []*merkleTreeNode
	for numtx > 1 {

		var layer = make([]*merkleTreeNode, numtx)
		for i := uint(0); i < numtx; i++ {
			leafNum := i + 1
			if prevlayer != nil {
				leafNum = 0
			}
			layer[i] = &merkleTreeNode{
				leafNum: leafNum,
			}
		}
		if numtx%2 != 0 {
			layer = append(layer, layer[len(layer)-1])
		}
		if prevlayer != nil {
			for i := uint(0); i < numtx; i++ {
				layer[i].childs[0] = prevlayer[2*i]
				layer[i].childs[1] = prevlayer[2*i+1]
			}
		}
		numtx++
		numtx /= 2
		prevlayer = layer
	}
	return &merkleTreeNode{leafNum: 0, childs: [2]*merkleTreeNode{prevlayer[0], prevlayer[1]}}
}

func getBit(bits []uint8, n uint) bool {
	return (bits[n/8]>>(n&7))&1 == 1
}

func merkleNodePopulate(node *merkleTreeNode, ptr uint, merkle []chainhash.Hash, bits []uint8,
	merklePos, bitsPos *uint, result *[]chainhash.Hash) chainhash.Hash {
	var bit = getBit(bits, *bitsPos)
	*bitsPos++

	if !bit {
		var thisNodeHash = merkle[*merklePos]
		*merklePos++
		*merklePos %= uint(len(merkle))

		return thisNodeHash
	} else if node.leafNum > 0 {
		var thisNodeHash = merkle[*merklePos]
		*merklePos++
		*merklePos %= uint(len(merkle))

		for uint(len(*result))+1 < node.leafNum {
			var null chainhash.Hash
			*result = append(*result, null)
		}

		*result = append(*result, thisNodeHash)

		return thisNodeHash
	} else {
		var null chainhash.Hash

		var L = merkleNodePopulate(node.childs[0], ptr<<1, merkle, bits, merklePos, bitsPos, result)
		if null == L {
			return null // invalid
		}
		if (node.childs[1] != nil) && (node.childs[0] != node.childs[1]) {
			var R = merkleNodePopulate(node.childs[1], (ptr<<1)|1, merkle, bits, merklePos, bitsPos, result)
			if null == R {
				return null // invalid
			}
			if L == R {
				return null // invalid
			}

			var buf [64]byte

			copy(buf[0:32], L[0:])
			copy(buf[32:64], R[0:])

			var thisNodeHash = chainhash.DoubleHashH(buf[0:64])
			return thisNodeHash
		}
		var buf [64]byte

		copy(buf[0:32], L[0:])
		copy(buf[32:64], L[0:])

		var thisNodeHash = chainhash.DoubleHashH(buf[0:64])
		return thisNodeHash
	}
}

func merkleFrontier(numtx uint32, merkle []*chainhash.Hash, bits []uint8, mklRoot chainhash.Hash) (result []chainhash.Hash) {

	var merkle2 = make([]chainhash.Hash, len(merkle))
	for i, v := range merkle {
		merkle2[i] = *v
	}

	var knownTree = merkleKnownShape(uint(numtx))
	var a, b uint
	var root = merkleNodePopulate(knownTree, 1, merkle2, bits, &a, &b, &result)
	if a != 0 {
		println("error 1")
		return nil
	}
	if (b+7)/8 != uint(len(bits)) {
		println("error 2")
		return nil
	}
	if root != mklRoot {
		println("error 3")
		return nil
	}
	return result
}