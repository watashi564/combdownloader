package main

import "net"
import "net/http"
import "time"
import "io"
import "encoding/json"
import "fmt"
import "encoding/hex"

type HexKey [32]byte

// MarshalJSON serializes ByteArray to hex
func (s HexKey) MarshalJSON() ([]byte, error) {
	bytes, err := json.Marshal(fmt.Sprintf("0020%x", s))
	return bytes, err
}

type TxId [32]byte

// MarshalJSON serializes ByteArray to hex
func (s TxId) MarshalJSON() ([]byte, error) {
	bytes, err := json.Marshal(fmt.Sprintf("%x", s))
	return bytes, err
}

type SPubKey struct {
	Type string `json:"type"`
	Hex  HexKey `json:"hex"`
}
type Input struct {
	Vout uint32 `json:"vout"`
	TxId TxId   `json:"txid"`
}
type Output struct {
	ScriptPubKey *SPubKey `json:"scriptPubKey,omitempty"`
	Value        float64  `json:"value,omitempty"`
}
type Transaction struct {
	Vout []Output `json:"vout"`
	Vin  []Input  `json:"vin,omitempty"`
	TxId TxId     `json:"txid,omitempty"`
}

type Block struct {
	Tx     []Transaction `json:"tx"`
	Height uint64        `json:"height"`
	Hash   string        `json:"hash"`
	hash   [32]byte
	PBH    string `json:"previousblockhash"`
	Time   int64  `json:"time"`
	pulled time.Time
}
type WaitForBlock struct {
	Hash   string `json:"hash"`
	Height int    `json:"height"`
}

type ChainTip struct {
	Hash   string `json:"hash"`
	Height int    `json:"height"`
	Status string `json:"status"`

	Branchlen int `json:"branchlen"`
}

func (b *Block) P2WSHCount() (c uint64) {

	if b.Tx == nil {
		return 0
	}

	for _, tx := range b.Tx {
		for _, out := range tx.Vout {
			if out.ScriptPubKey != nil && out.ScriptPubKey.Type == "witness_v0_scripthash" {
				c++
			}
		}
	}
	return
}

func (req *Request) dispatch(w http.ResponseWriter) (r *Response) {
	switch req.Method {
	case "getbestblockhash":
		//spew.Dump(req)
		r = new(Response)
		r.Result = bchain.getbestblockhash()
	case "getblockcount":
		//spew.Dump(req)
		r = new(Response)
		r.Result = bchain.getblockcount()
	case "getblockhash":
		//fmt.Println(req.Params[0])
		r = new(Response)
		r.Result = bchain.getblockhash(int(req.Params[0].(float64)))
	case "getblock":
		//fmt.Println(req)
		r = new(Response)
		r.Result = bchain.getblock(req.Params[0].(string))
	case "waitfornewblock":
		var timeMs = (int(req.Params[0].(float64)))
		r = new(Response)
		newblock := new(WaitForBlock)
		height, hash := bchain.getblockcountandhash()
		for ; timeMs > 0; timeMs-- {
			time.Sleep(time.Millisecond)
			newblock.Height, newblock.Hash = bchain.getblockcountandhash()
			if hash != newblock.Hash || height != newblock.Height {
				break
			}
		}
		//fmt.Println(newblock)
		r.Result = newblock
	case "getchaintips":
		var tip = []ChainTip{{
			Status:    "active",
			Branchlen: 0,
		}}
		r = new(Response)
		tip[0].Height, tip[0].Hash = bchain.getblockcountandhash()
		r.Result = tip
		//fmt.Println(tip)
	default:

	}
	return
}

type Response struct {
	Result interface{} `json:"result"`
	Error  *struct{}   `json:"error"`
	Id     *struct{}   `json:"id"`
}

type Request struct {
	Method string        `json:"method"`
	Params []interface{} `json:"params"`
}

func main_page(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	req_bytes, err := io.ReadAll(r.Body)
	if err != nil {
		println("cannot read all")
		return
	}

	var req Request

	err = json.Unmarshal(req_bytes, &req)
	if err != nil {
		println("cannot unmarshal")
		return
	}
	resp := req.dispatch(w)
	if resp == nil {
		println("no response")
		return
	}
	data, err := json.Marshal(resp)
	if err != nil {
		println("no json")
		return
	}

	//fmt.Printf("%s\n", data)
	_, err = w.Write(data)
	if err != nil {
		println("cannot write")
		return
	}
}

type blockchain struct {
}

func (chain *blockchain) getblock(hash string) *Block {

	var buf [32]byte

	_, err3 := hex.Decode(buf[:], []byte(hash))
	if err3 != nil {
		return nil
	}

	height := myBlkHeightOfBlock(buf)

	return PullAcceleratedBlockOnline(buf, height)
}
func (chain *blockchain) getblockcount() int {

	myMutex.RLock()
	defer myMutex.RUnlock()

	//println("*myActiveTotalView:", *myActiveTotalView)

	return int(*myActiveTotalView)
}

func (chain *blockchain) getblockcountandhash() (int, string) {

	myMutex.RLock()
	defer myMutex.RUnlock()
	if *myActiveTotalView >= uint64(len(*myActiveChain)) {
		return int(len(*myActiveChain) - 1), (*myActiveChain)[len(*myActiveChain)-1].String()
	}

	//println("*myActiveTotalView:", *myActiveTotalView)

	return int(*myActiveTotalView), (*myActiveChain)[*myActiveTotalView].String()
}

func (chain *blockchain) getblockhash(height int) string {
	myMutex.RLock()
	defer myMutex.RUnlock()

	if *myActiveTotalView < uint64(height) {
		return ""
	}
	return (*myActiveChain)[uint64(height)].String()
}
func (chain *blockchain) getbestblockhash() string {
	myMutex.RLock()
	defer myMutex.RUnlock()

	if *myActiveTotalView >= uint64(len(*myActiveChain)) {
		return (*myActiveChain)[len(*myActiveChain)-1].String()
	}

	return (*myActiveChain)[*myActiveTotalView].String()
}

var bchain blockchain

type server struct {
}

func (server) ServeHTTP(rw http.ResponseWriter, hr *http.Request) {
	main_page(rw, hr)
}

func Serve() {
	err := serve()
	if err != nil {
		fmt.Println(err.Error())
	}
}
func serve() error {

	bind := "127.0.0.1:8332"

	if ConfigTestnet() {
		bind = "127.0.0.1:18332"
	}
	if ConfigRegtest {
		bind = "127.0.0.1:18443"
	}

	// Setup Listen
	ln, err6 := net.Listen("tcp", bind)
	if err6 != nil {
		println(err6.Error())
		return err6
	}

	srv := &http.Server{
		Handler:      server{},
		WriteTimeout: 20 * time.Second,
		ReadTimeout:  20 * time.Second,
	}

	err := srv.Serve(ln)
	if err != nil {
		println(err.Error())
		return err
	}
	return nil
}