package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "github.com/btcsuite/btcd/wire"
import "net"
import "time"
import "log"
import "encoding/binary"
import "fmt"
import "golang.org/x/net/netutil"

//import "github.com/davecgh/go-spew/spew"

// Listen wraps net.Listen with netutil.LimitListener to limit concurrent connections.
func Listen(network string, address string, connectionLimit int) (net.Listener, error) {
	unlimitedListener, err := net.Listen(network, address)
	if err != nil {
		return nil, err
	}
	return netutil.LimitListener(unlimitedListener, connectionLimit), nil
}

func bind_clean_up(bind string) string {
	for i := range bind {
		if bind[i] == '/' {
			return bind[0:i]
		}
	}
	return bind
}
func bind_conn_count(bind string) int {
	for i := range bind {
		if bind[i] == '/' {
			intValue := 0
			_, err := fmt.Sscan(bind_clean_up(bind[i+1:]), &intValue)
			if err != nil || intValue <= 0 {
				break
			}
			return intValue
		}
	}
	return 1000
}

func uploader_main(bind string) {
	listener, err := Listen("tcp", bind_clean_up(bind), bind_conn_count(bind))
	if err != nil {
		log.Printf("Connection error: %s\n", err)
		return
	} else {
		log.Println("Listening")
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go func(conn net.Conn) {
			err = uploader_connection(conn)
			log.Println(err)
			conn.Close()
		}(conn)
	}
}

func uploader_connection(conn net.Conn) error {
	pver := wire.AddrV2Version
	var minBullshitMessagesAcceptedVersion = true
	_ = minBullshitMessagesAcceptedVersion
	btcnet := wire.MainNet
	if ConfigRegtest {
		btcnet = wire.TestNet
	}
	if ConfigTestnet() {
		btcnet = wire.TestNet3
	}

	fn := new(FullNode)
	fn.conn = &conn
	fn.pver = pver
	fn.btcnet = btcnet

	var filterLoad *wire.MsgFilterLoad
	var sers wire.ServiceFlag

	defer blockProxyMapDeleteRecipient(fn)

	for {
		_, msg, _, err := wire.ReadMessageWithEncodingN(conn, pver, btcnet, wire.WitnessEncoding)
		if minBullshitMessagesAcceptedVersion && err != nil && err.Error() == "received unknown message" {
			// in case someone will send us an unknown message in the main loop
			err = nil
		}
		if err != nil {
			log.Println(err)
			return err
		}

		switch tmsg := msg.(type) {

		case *wire.MsgVersion:

			var respService = wire.ServiceFlag(ServiceCombNode)
			if uint64(tmsg.Services) == ServiceCombNodeBloomClient {
				respService = wire.ServiceFlag(ServiceCombNodeBloomClient)
			}
			if uint64(tmsg.Services) == ServiceCombNodeClient {
				respService = wire.ServiceFlag(ServiceCombNodeClient)
			}
			// Send initial version handshake.
			nonce, err := wire.RandomUint64()
			if err != nil {
				log.Println(err)
				return err
			}
			msgVersion := NewMsgVersionFromConn(conn, nonce, int32(myCount()), respService)
			msgVersion.DisableRelayTx = true
			msgVersion.UserAgent = "/"

			minBullshitMessagesAcceptedVersion = uint32(tmsg.ProtocolVersion) >= wire.AddrV2Version
			// Negotiate protocol version.
			if uint32(tmsg.ProtocolVersion) < pver {
				pver = uint32(tmsg.ProtocolVersion)
			}
			WiringMutex.Lock()
			err = func() error {
				if err := msgVersion.AddUserAgent(UserAgentName, UserAgentVersion); err != nil {
					log.Println(err)
					return err
				}
				if err := wire.WriteMessage(conn, msgVersion, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				if minBullshitMessagesAcceptedVersion {
					// send sendaddrv2 - we are ready
					if err := wire.WriteMessage(conn, wire.NewMsgSendAddrV2(), pver, btcnet); err != nil {
						log.Println(err)
						return err
					}
				}
				msgVerAck := wire.NewMsgVerAck()
				if err := wire.WriteMessage(conn, msgVerAck, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				if myChainIsActive() {
					var top, _ = myTop()
					msgInv := wire.NewMsgInv()
					msgInv.InvList = append(msgInv.InvList, &wire.InvVect{
						Type: wire.InvTypeWitnessBlock,
						Hash: *top,
					})
					if err := wire.WriteMessage(conn, msgInv, pver, btcnet); err != nil {
						log.Println(err)
						return err
					}
				}
				return nil
			}()
			WiringMutex.Unlock()
			if err != nil {
				log.Println(err)
				return err
			}
			sers = tmsg.Services

		case *wire.MsgVerAck:

			if sers&wire.SFNodeWitness == 0 {
				PutFullNode(fn, FullNodeAcceleratorBloom|FullNodePingable|FullNodeUploader)
				defer DeleteFullNode(fn)
			} else if sers&wire.SFNodeBloom != 0 {
				PutFullNode(fn, FullNodeUploader|FullNodeSegDownloadable|FullNodePingable|FullNodeDownloadableAndBloomable|FullNodeDownloadable)
				defer DeleteFullNode(fn)
			} else {
				PutFullNode(fn, FullNodeUploader|FullNodeSegDownloadable|FullNodePingable|FullNodeDownloadable)
				defer DeleteFullNode(fn)
			}

		case *wire.MsgGetAddr:

			msgAddrV2 := wire.NewMsgAddr()

			for _, nodstr := range ConfigGossip {

				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				if net.ParseIP(addr.String()) == nil {
					continue
				}
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, &wire.NetAddress{
					Timestamp: time.Now(),
					Services:  wire.ServiceFlag(ServiceCombNode),
					IP:        net.ParseIP(addr.String()),
					Port:      addr.Port,
				})
			}

			var prevstr string

			for i := 0; i < 1000-len(ConfigGossip); i++ {
				servenod, nodstr := MapRandomNode()
				if servenod == nil {
					continue
				}
				if nodstr == prevstr {
					break
				}

				prevstr = nodstr
				//println(nodstr)
				addr := ToNetString(nodstr)
				if addr == nil {
					continue
				}

				//println(addr.String())

				if net.ParseIP(addr.String()) == nil {
					continue
				}

				msgAddrV2.AddrList = append(msgAddrV2.AddrList, &wire.NetAddress{
					Timestamp: time.Now(),
					Services:  wire.ServiceFlag(servenod.Services),
					IP:        net.ParseIP(addr.String()),
					Port:      addr.Port,
				})
			}

			WiringMutex.Lock()
			err := func() error {
				if err := wire.WriteMessage(conn, msgAddrV2, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				return nil
			}()
			WiringMutex.Unlock()
			if err != nil {
				log.Println(err)
				return err
			}

		case *wire.MsgSendAddrV2:

			msgAddrV2 := wire.NewMsgAddrV2()
			for _, nodstr := range ConfigGossip {

				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, wire.NetAddressV2FromBytes(time.Now(),
					wire.ServiceFlag(ServiceCombNode), addr.Bytes, addr.Port))
			}
			var prevstr string

			for i := 0; i < 1000-len(ConfigGossip); i++ {
				servenod, nodstr := MapRandomNode()
				if servenod == nil {
					continue
				}
				if nodstr == prevstr {
					break
				}

				prevstr = nodstr
				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, wire.NetAddressV2FromBytes(time.Now(),
					wire.ServiceFlag(servenod.Services), addr.Bytes, addr.Port))
			}

			WiringMutex.Lock()
			err := func() error {
				if err := wire.WriteMessage(conn, msgAddrV2, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				return nil
			}()
			WiringMutex.Unlock()
			if err != nil {
				log.Println(err)
				return err
			}

		case *wire.MsgGetHeaders:
			//log.Printf("Received MsgGetHeaders: %T", tmsg)
			err, ok := nonstandard_accelerator_headers_do(conn, tmsg, pver, btcnet)
			if err != nil {
				log.Println(err)
				return err
			}
			if !ok {

				headersHeight, _ := generic_headers_height(tmsg)
				if headersHeight == ^uint64(0) {
					return errBadGetHeaders
				}

				err := generic_headers_delivery(conn, headersHeight, pver, btcnet)
				if err != nil && err != errChainInactive {
					log.Println(err)
					return err
				}

			}
		case *wire.MsgPing:

			msgPong := wire.NewMsgPong(tmsg.Nonce)

			WiringMutex.Lock()
			err := func() error {

				if err := wire.WriteMessage(conn, msgPong, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				return nil
			}()
			WiringMutex.Unlock()
			if err != nil {
				log.Println(err)
				return err
			}

		case *wire.MsgPong:

			fn.hadMut.Lock()
			fn.kick = fn.nonce != tmsg.Nonce
			fn.hadMut.Unlock()

		case *wire.MsgFilterClear:

			filterLoad = nil

		case *wire.MsgFilterLoad:

			filterLoad = tmsg

			err := nonstandard_bloom_filter_do(conn, tmsg, pver, btcnet)
			if err != nil {
				log.Println(err)
				return err
			}

			if tmsg.HashFuncs == 0 && tmsg.Tweak == 0 && tmsg.Flags == 0 && len(tmsg.Filter) >= 40 {

				var hash [32]byte
				copy(hash[:], tmsg.Filter[0:32])
				var count = binary.BigEndian.Uint64(tmsg.Filter[32:40])
				if is, smaller := acceleratorBloomExist(hash, count, uint64(len(tmsg.Filter)-40)); is && smaller {

					// extra check when replacing accelerator

					println("get block")

					blk := PullAcceleratedBlock(hash, tmsg.Filter[40:])
					if blk == nil {
						continue
					}
					var cnt = blk.P2WSHCount()
					println("cnt", cnt)

					if cnt != count {
						continue
					}
				} else if is != smaller {
					// don't use scenario inside acceleratorBloomExist
					continue
				}

				NotifyBloom(tmsg.Filter[0:40], tmsg.Filter[40:])
			}

		case *wire.MsgAddr:
			if ConfigHaveExtranodes() {
				for _, addr := range tmsg.AddrList {

					if is_bad_port(addr.Port) {
						continue
					}

					if ConfigForceBloom && addr.Services&wire.SFNodeBloom == 0 {
						continue
					}

					if isIpV6(addr.IP.String()) {
						AddRoamingNode(fmt.Sprintf("[%s]:%d", addr.IP.String(), addr.Port), uint64(addr.Services))
					} else {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.IP.String(), addr.Port), uint64(addr.Services))
					}
				}
			}
			continue
		case *wire.MsgAddrV2:
			if ConfigHaveExtranodes() {
				for _, addr := range tmsg.AddrList {

					if is_bad_port(addr.Port) {
						continue
					}

					if ConfigForceBloom && addr.Services&wire.SFNodeBloom == 0 {
						continue
					}

					if addr.IsTorV3() {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else if isIpV6(addr.Addr.String()) {
						AddRoamingNode(fmt.Sprintf("[%s]:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					}
				}
			}
		case *wire.MsgHeaders:

			nonstandard_receive_accelerators(tmsg)

		case *wire.MsgGetData:
			// get block / filtered block
			//spew.Dump(tmsg)
			go generic_block_delivery(fn, tmsg.InvList, filterLoad)

		case *wire.MsgMerkleBlock:
			proxy_merkle(tmsg)
		case *wire.MsgBlock:
			proxy_block(tmsg)
		case *wire.MsgTx:
			proxy_tx(tmsg)
		case *wire.MsgSendHeaders:

			var top, _ = myTop()
			if top != nil {
				err := fn.Invite(*top)
				if err != nil {
					fmt.Println(err)
				}
			}

		case *wire.MsgNotFound:

			var map1 = make(map[*FullNode][]*wire.InvVect)

			for _, v := range tmsg.InvList {
				if v == nil {
					continue
				}
				var hsh = (v.Hash)
				var nodes = blockProxyMapGet(hsh)
				if len(nodes) == 0 {
					continue
				}
				var height1 = myBlkHeight(&hsh)
				var height2 = myBlkHeightOfBlock(hsh)
				if (height1 == 0) == (height2 == 0) {
					continue
				}
				var height = height1 | height2

				fn.hadMut.Lock()
				if height > fn.noSuchBlock {
					fn.noSuchBlock = height
				}
				fn.hadMut.Unlock()
				for _, n := range nodes {
					map1[n] = append(map1[n], v)
				}
			}

			for k, v := range map1 {
				go generic_block_delivery(k, v, nil)
			}

		default:
			//spew.Dump(msg)
		}
	}
}

func generic_block_delivery(fn *FullNode, invList []*wire.InvVect, filterLoad *wire.MsgFilterLoad) {
	var blocks = -1
	var nodes = make(map[[32]byte]*FullNode)
	for blocks != 0 {
		//fmt.Println(blocks, len(nodes))
		for _, iv := range invList {
			if iv == nil {
				continue
			}
			if iv.Type != wire.InvTypeBlock && iv.Type != wire.InvTypeFilteredBlock && iv.Type != wire.InvTypeWitnessBlock {
				continue
			}

			if iv.Type == wire.InvTypeFilteredBlock && filterLoad == nil {
				continue
			}
			var hsh = (iv.Hash)
			var height1 = myBlkHeight(&hsh)
			var height2 = myBlkHeightOfBlock(hsh)
			if (height1 == 0) == (height2 == 0) {
				continue
			}
			var height = height1 | height2
			fn.hadMut.Lock()
			if fn.pulledBlock < height {
				fn.pulledBlock = height
			}
			fn.hadMut.Unlock()

			if blocks != -1 {
				if !blockProxyMapGetExactly([32]byte(hsh), fn) {
					delete(nodes, [32]byte(hsh))
					continue
				} else {
					var nod = nodes[[32]byte(hsh)]
					if nod != nil {
						//fmt.Println("DC", nod.Addr)
						nod.BlockFail(height, 10)
					}
					delete(nodes, [32]byte(hsh))
				}
			}

			for i := 0; i < 10000; i++ {
				time.Sleep(time.Millisecond)
				var services byte = FullNodeDownloadable
				if iv.Type == wire.InvTypeFilteredBlock {
					services = FullNodeDownloadableAndBloomable
				}
				if iv.Type == wire.InvTypeWitnessBlock && height >= 481824 {
					services |= FullNodeSegDownloadable
				}

				var node = GetFullNode(services)

				if node == nil {
					continue
				}
				if node == fn {
					continue
				}
				if node.conn == fn.conn {
					continue
				}

				node.hadMut.Lock()
				var nodeblock = node.pulledBlock
				var minnodeblock = node.noSuchBlock
				node.hadMut.Unlock()

				// only send blocks from node that is ahead by 100 blocks to node that is behind
				// wiggle room 100 to avoid requesting blocks from self
				if (nodeblock != 0 && nodeblock < height+100) || (minnodeblock >= height) {
					continue
				}

				//fmt.Println("GET", node.Addr, nodeblock, height)
				if iv.Type == wire.InvTypeFilteredBlock {
					err := downloader_download_filtered_block2(node.conn, node.pver, node.btcnet, &hsh, filterLoad)
					if err != nil {
						log.Println(err)
						continue
					}
					nodes[[32]byte(hsh)] = node
					blockProxyMapAdd([32]byte(hsh), fn)
				} else if iv.Type == wire.InvTypeBlock {
					err := downloader_download_block(node.conn, node.pver, node.btcnet, &hsh)
					if err != nil {
						log.Println(err)
						continue
					}
					nodes[[32]byte(hsh)] = node
					blockProxyMapAdd([32]byte(hsh), fn)
				} else if iv.Type == wire.InvTypeWitnessBlock {
					msg := wire.NewMsgGetDataSizeHint(1)
					err := msg.AddInvVect(iv)
					if err != nil {
						log.Println(err)
						continue
					}
					err = deliver_block_node(node.conn, node.pver, node.btcnet, msg)
					if err != nil {
						log.Println(err)
						continue
					}
					nodes[[32]byte(hsh)] = node
					blockProxyMapAdd([32]byte(hsh), fn)
				}

				break
			}
		}
		if len(nodes) == 0 {
			break
		}
		blocks = blockProxyMapBlocksLen(fn)
		time.Sleep(time.Duration(1+blocks) * 100 * time.Millisecond)
	}
}

func generic_headers_height(msg *wire.MsgGetHeaders) (uint64, chainhash.Hash) {

	var maxHeight uint64
	var maxHdr chainhash.Hash
	for _, hdr2 := range msg.BlockLocatorHashes {
		var height2 = myBlkHeight(hdr2)
		maxHeight = height2
		maxHdr = *hdr2
		if maxHeight != 0 {
			break
		}
	}

	if maxHeight == 0 && len(msg.BlockLocatorHashes) > 1 {
		return ^uint64(0), maxHdr
	}

	return maxHeight, maxHdr
}

func generic_headers_delivery(conn net.Conn, maxHeight uint64, pver uint32, btcnet wire.BitcoinNet) error {
	msgHeaders := wire.NewMsgHeaders()
	//var maxths chainhash.Hash
	for h := maxHeight + 1; h <= maxHeight+2000; h++ {
		ver, time32, nonce, bits, mkl, prev, ths := myGetAll(h)
		if mkl == [32]byte{} || prev == [32]byte{} {
			//maxths = ths
			break
		}
		var hdr = &wire.BlockHeader{
			Version:    int32(ver),
			PrevBlock:  prev,
			MerkleRoot: mkl,
			Timestamp:  time.Unix(int64(time32), 0),
			Bits:       bits,
			Nonce:      nonce,
		}
		if hdr.BlockHash() == ths {
			err := msgHeaders.AddBlockHeader(hdr)
			if err != nil {
				return err
			}
		} else {
			log.Println("tried to send invalid block hdr:", hdr.BlockHash(), ths, hdr)
			//spew.Dump(hdr.BlockHash(), ths, hdr)
			break
		}
	}

	if !myChainIsInitiallyActive() {
		return errChainInactive
	}
	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(conn, msgHeaders, pver, btcnet); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func nonstandard_accelerator_headers_do(conn net.Conn, msg *wire.MsgGetHeaders, pver uint32, btcnet wire.BitcoinNet) (error, bool) {

	var maxHdr [32]byte
	var minHdr [32]byte
	if len(msg.BlockLocatorHashes) == 0 {
		return nil, false
	}
	if len(msg.BlockLocatorHashes) > 2 {
		return nil, false
	}
	if len(msg.BlockLocatorHashes) == 2 {
		minHdr = [32]byte(*msg.BlockLocatorHashes[1])
	}
	if len(msg.BlockLocatorHashes) <= 2 {
		maxHdr = [32]byte(*msg.BlockLocatorHashes[0])
	}
	var acc, blk, count, ok = acceleratorHeader(maxHdr)
	if !ok {
		return nil, false
	}
	msgHeaders := wire.NewMsgHeaders()
	for i := 0; i < 2000; i++ {

		var hdr = &wire.BlockHeader{
			PrevBlock:  blk,
			MerkleRoot: acc,
			Bits:       uint32(count >> 32),
			Nonce:      uint32(count),
			Timestamp:  time.Unix(0, 0),
			Version:    0,
		}
		err := msgHeaders.AddBlockHeader(hdr)
		if err != nil {
			println(err.Error())
			break
		}
		if acc == minHdr {
			break
		}
		acc, blk, count, ok = acceleratorHeader(acc)
		if !ok {
			break
		}
		if acc == [32]byte{} {
			break
		}
	}

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(conn, msgHeaders, pver, btcnet); err != nil {
		log.Println(err)
		return err, true
	}
	return nil, true
}

func nonstandard_bloom_filter_do(conn net.Conn, msg *wire.MsgFilterLoad, pver uint32, btcnet wire.BitcoinNet) error {

	if len(msg.Filter) != 32 {
		return nil
	}

	var hash [40]byte
	var hsh [32]byte
	copy(hash[0:32], msg.Filter[0:32])
	copy(hsh[0:32], hash[0:32])

	var count = uint64(msg.Tweak) | (uint64(msg.HashFuncs) << 32)
	binary.BigEndian.PutUint64(hash[32:40], count)

	var buffer = hash[:]

	var getted = acceleratorGetCnt(hsh, count)
	if getted == nil {
		return nil
	}

	buffer = append(buffer, getted...)

	var msgData = wire.NewMsgFilterLoad(buffer, 0, 0, 0)

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	if err := wire.WriteMessage(conn, msgData, pver, btcnet); err != nil {
		log.Println(err)
		return err
	}
	return nil

}

func proxy_tx(tmsg *wire.MsgTx) {
	var bh = tmsg.TxHash()

	var recipients = blockProxyMapGet(bh)
	for _, fn := range recipients {
		err := deliver_block_node(fn.conn, fn.pver, fn.btcnet, tmsg)
		if err != nil {
			log.Println(err)
			continue
		}
	}

	blockProxyMapDeleteBlock(bh)
}

func proxy_merkle(tmsg *wire.MsgMerkleBlock) {
	var bh = tmsg.Header.BlockHash()
	var transactions []chainhash.Hash
	var recipients = blockProxyMapGet(bh)
	for _, fn := range recipients {
		err := deliver_block_node(fn.conn, fn.pver, fn.btcnet, tmsg)
		if err != nil {
			log.Println(err)
			continue
		}
		if transactions == nil {
			transactions = merkleFrontier(tmsg.Transactions, tmsg.Hashes, tmsg.Flags, tmsg.Header.MerkleRoot)
		}
		for _, tx := range transactions {
			blockProxyMapAdd([32]byte(tx), fn)
		}
	}

	blockProxyMapDeleteBlock(bh)
}

func proxy_block(tmsg *wire.MsgBlock) {
	var bh = tmsg.Header.BlockHash()

	//spew.Dump(bh)

	var recipients = blockProxyMapGet(bh)
	for _, fn := range recipients {
		err := deliver_block_node(fn.conn, fn.pver, fn.btcnet, tmsg)
		if err != nil {
			log.Println(err)
			continue
		}
	}

	blockProxyMapDeleteBlock(bh)
}