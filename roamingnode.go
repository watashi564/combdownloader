package main

import "log"
import "time"
import "math/rand"
import "sync"
import "strings"
import "encoding/base32"
import "net"
import "hash/crc64"

var hashtable *crc64.Table

func init() {
	hashtable = crc64.MakeTable(crc64.ECMA)
}

func hash(str string) uint64 {
	return crc64.Checksum([]byte(str), hashtable)
}

type NetString struct {
	Net   byte
	Str   string
	Port  uint16
	Bytes []byte
}

const IPV4 = 1
const IPV6 = 2
const TORV3 = 4

func ToNetString(str string) (ns *NetString) {
	ns = new(NetString)
	if isTor(str) {
		ns.Net = TORV3
	} else if isIpV6(str) {
		ns.Net = IPV6
	} else {
		ns.Net = IPV4
	}
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] >= '0' && str[i] <= '9' {
			continue
		}
		if str[i] == ':' {
			for _, c := range str[i:] {
				if c >= '0' && c <= '9' {
					ns.Port *= 10
					ns.Port += uint16(c - '0')
					continue
				}
			}
			str = str[0:i]
		}
		break
	}
	if len(str) > 1 && str[0] == '[' && str[len(str)-1] == ']' {
		str = str[1 : len(str)-1]
	}
	if len(str) == 62 {
		str = str[0:56] //strip onion
		str = strings.ToUpper(str)
	}
	ns.Str = str
	switch ns.Net {
	case IPV4:
		fallthrough
	case IPV6:
		ns.Bytes = net.ParseIP(ns.Str)
	case TORV3:
		bytes, err := base32.StdEncoding.DecodeString(ns.Str)
		if err != nil {
			return nil
		}
		if len(bytes) > 32 {
			ns.Bytes = bytes[0:32]
		} else {
			return nil
		}
	default:
		return nil
	}
	return ns
}

func (n *NetString) String() string {
	return n.Str
}

type RoamingNode struct {
	fn   *FullNode
	Busy bool

	Services uint64

	connectedTimes uint64
}

func (rn *RoamingNode) ConnectedEvent() {
	rn.connectedTimes++
}

func RunForeverRoaming(combnode bool) {
outer:
	for {
		rn, addr := MapRandomNode()
		if rn == nil {
			time.Sleep(time.Second)
			continue
		}

		for combnode && (rn.Services != ServiceCombNode) {
			time.Sleep(time.Millisecond)
			rn2, addr2 := MapRandomNode()
			if rn2 == nil {
				time.Sleep(time.Second)
				continue outer
			}
			if addr2 == addr || rn2 == rn {
				log.Println("Getting addr")
				AddrFullNodes()
				time.Sleep(time.Second)
				continue outer
			}
			rn, addr = rn2, addr2
		}

		if !rn.Acquire() {
			time.Sleep(time.Millisecond)
			continue
		}

		rn.fn = new(FullNode)
		rn.fn.Addr = addr

		if rn.connectedTimes <= RoamingCrawlNumber(8) {

			err := rn.fn.run(false, combnode)
			if err != nil {
				if isUnavailable(err) {
					BanRoamingNode(addr)
				} else {
					log.Println(err)
				}
			}
		}
		// else {
		//
		//	num := RoamingCrawlNumber(100000)
		//	log.Printf("[CRAWL] %d.%03d%%\n", num/1000, num%1000)
		//}
		blockProxyMapDeleteRecipient(rn.fn)
		rn.fn = nil
		rn.Release()
	}
}

func (rn *RoamingNode) Release() {
	nodeMapMutex.Lock()
	rn.Busy = false
	nodeMapMutex.Unlock()

}
func (rn *RoamingNode) Acquire() bool {

	nodeMapMutex.RLock()

	if rn.Busy {
		nodeMapMutex.RUnlock()
		return false
	}

	nodeMapMutex.RUnlock()

	nodeMapMutex.Lock()

	if rn.Busy {
		nodeMapMutex.Unlock()
		return false
	}
	rn.Busy = true

	nodeMapMutex.Unlock()

	return true
}

var nodeMapMutex sync.RWMutex
var nodeMap = make(map[string]*RoamingNode)
var bannedNodeMap = make(map[uint64]struct{})
var nodeTotalConnectedTimes uint64

// Eventually CrawlNumber will overflow to 1
// (when we connect to all the nodes in the visible portion of the network)
// At that moment we will start reconnecting to unhelpful nodes
func RoamingCrawlNumber(mul uint64) (crawlId uint64) {
	nodeMapMutex.Lock()
	crawlId = mul * uint64(nodeTotalConnectedTimes) / (uint64(len(nodeMap)) + 1 + uint64(len(bannedNodeMap)))
	nodeMapMutex.Unlock()
	return crawlId
}

// If node was helpful we clear it's connected counter to zero to use it more often
// But we don't decrease the nodeTotalConnectedTimes to make sure the crawl ends
func RoamingNodeWasHelpful(addr string) {
	nodeMapMutex.Lock()
	if node := nodeMap[addr]; node != nil {
		node.connectedTimes = 0
	}
	nodeMapMutex.Unlock()
}

func RoamingNodeWasConnected(addr string) {
	nodeMapMutex.Lock()
	if node := nodeMap[addr]; node != nil {
		node.connectedTimes++
		nodeTotalConnectedTimes++
	}
	nodeMapMutex.Unlock()
}
func RoamingNodeBanned(str string) (isBanned bool) {
	nodeMapMutex.RLock()
	_, isBanned = bannedNodeMap[hash(str)]
	nodeMapMutex.RUnlock()
	return isBanned
}
func BanRoamingNode(str string) {
	nodeMapMutex.Lock()
	if _, ok := nodeMap[str]; ok {

		// delete one if exceeds cap
		if ConfigBannedNodesCap > 0 {
			if len(bannedNodeMap) > ConfigBannedNodesCap {
				for k := range bannedNodeMap {
					delete(bannedNodeMap, k)
					break
				}
			}
		}

		bannedNodeMap[hash(str)] = struct{}{}
		delete(nodeMap, str)
	}
	nodeMapMutex.Unlock()
}
func SometimesHalveBanTable() {
	nodeMapMutex.Lock()
	var n uint64
	for k := range bannedNodeMap {
		n++
		if n&1 == 0 {
			delete(bannedNodeMap, k)
		}
	}
	nodeMapMutex.Unlock()
}

func AddRoamingNode(str string, services uint64) {

	if RoamingNodeBanned(str) {
		return
	}

	nodeMapMutex.Lock()

	var nod = new(RoamingNode)

	nod.Services = services

	// delete one if exceeds cap
	if ConfigNodesCap > 0 {
		if len(nodeMap) > ConfigNodesCap {
			for k := range nodeMap {
				delete(nodeMap, k)
				break
			}
		}
	}

	nodeMap[str] = nod

	nodeMapMutex.Unlock()
}
func MapFewNodes() bool {
	nodeMapMutex.RLock()
	defer nodeMapMutex.RUnlock()
	return len(nodeMap) < 10
}

func isUnavailable(err error) bool {
	if err == nil {
		return false
	}
	var s = err.Error()
	if strings.HasSuffix(s, "timeout") {
		return true
	}
	if strings.HasSuffix(s, "connection timed out") {
		return true
	}
	if strings.HasSuffix(s, "unreachable") {
		return true
	}
	if strings.HasSuffix(s, "connection refused") {
		return true
	}
	if strings.HasSuffix(s, "no route to host") {
		return true
	}
	return false
}

func MapRandomNode() (rn *RoamingNode, addr string) {
	nodeMapMutex.RLock()
	defer nodeMapMutex.RUnlock()

	if len(nodeMap) == 0 {
		return nil, ""
	}

	var i = rand.Intn(len(nodeMap))
	for k, v := range nodeMap {
		i--
		if i < 0 {
			return v, k
		}
	}
	return nil, ""
}