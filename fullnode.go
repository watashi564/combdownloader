package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "github.com/btcsuite/btcd/wire"
import "net"
import "log"
import "io"
import "golang.org/x/net/proxy"
import "time"
import "fmt"
import "sync"
import "strings"
import "context"

type FullNode struct {
	conn   *net.Conn
	Addr   string
	pver   uint32
	btcnet wire.BitcoinNet
	getnod time.Time

	hadMut      sync.Mutex // protects newHdr, nonce, kick
	newHdr      [32]byte
	nonce       uint64
	kick        bool
	pulledBlock uint64
	noSuchBlock uint64
	blockFails  uint8

	checkpointHeight uint64
}

func (f *FullNode) Disconnect() {
	if f == nil {
		return
	}

	blockProxyMapDeleteRecipient(f)

	//println("disconnecting")
	(*f.conn).Close()
}

func (f *FullNode) BlockFail(height uint64, maxFails uint8) {
	if f == nil {
		return
	}

	f.hadMut.Lock()
	if f.noSuchBlock >= height {
		f.blockFails++
	}
	var fails = f.blockFails
	f.hadMut.Unlock()

	if fails >= maxFails {
		if !f.IsForever() {
			f.Disconnect()
		}
	}
}

func (f *FullNode) HeaderSyncOver() bool {
	return myChainIsActive()
}
func (f *FullNode) RunForever(syncheaders bool) {

	var combnode = true

	f.AddForever()
	for i := 0; true; i++ {
		f.hadMut.Lock()
		if syncheaders {
			f.Addr = ConfigMyBtcFullNode(i)
		}
		f.kick = false
		f.hadMut.Unlock()

		err := f.run(syncheaders, combnode)
		if err != nil {
			if err == errCombNodeForced {
				if combnode {
					i--
				}
				combnode = false
			}
			log.Println(err)
		}
		time.Sleep(1 * time.Second)
	}
}

var ForeverNodesMutex sync.RWMutex
var ForeverNodes = make(map[*FullNode]bool)

func (f *FullNode) AddForever() {
	ForeverNodesMutex.Lock()
	ForeverNodes[f] = true
	ForeverNodesMutex.Unlock()
}

func (f *FullNode) IsForever() bool {
	ForeverNodesMutex.RLock()
	defer ForeverNodesMutex.RUnlock()
	return ForeverNodes[f]
}

func (f *FullNode) run(syncheaders, combnode bool) error {

	pver := wire.ProtocolVersion
	var minBullshitMessagesAcceptedVersion = pver >= wire.AddrV2Version
	var extraBits byte

	btcnet := wire.MainNet
	if ConfigRegtest {
		btcnet = wire.TestNet
	}
	if ConfigTestnet() {
		btcnet = wire.TestNet3
	}

	f.pver = pver
	f.btcnet = btcnet

	log.Printf("Connecting to %s\n", f.Addr)

	var conn net.Conn
	var err error

	var nodeTimerMult time.Duration = 5
	if combnode {
		nodeTimerMult = 1
	}

	if isTor(f.Addr) || ConfigForceTor() {
		dialer, err := proxy.SOCKS5("tcp", ConfigTor(), nil, nil)
		if err != nil {
			log.Println(err)
			return err
		}
		ctx, cancel := context.WithTimeout(context.Background(), nodeTimerMult*3*time.Second)
		defer cancel()
		conn, err = (dialer.(proxy.ContextDialer)).DialContext(ctx, "tcp", f.Addr)
		if err != nil {
			log.Println(err)
			return err
		}
		defer conn.Close()
	} else {
		conn, err = net.DialTimeout("tcp", f.Addr, nodeTimerMult*2*time.Second)
		if err != nil {
			log.Println(err)
			return err
		}
		defer conn.Close()
	}

	RoamingNodeWasConnected(f.Addr)

	log.Printf("Connected to %s\n", f.Addr)

	f.conn = &conn

	// Send initial version handshake.
	nonce, err := wire.RandomUint64()
	if err != nil {
		log.Println(err)
		return err
	}
	var respService wire.ServiceFlag
	if ConfigIsBloom() {
		if combnode {
			respService = wire.ServiceFlag(ServiceCombNodeBloomClient)
		} else {
			respService = wire.ServiceFlag(ServiceCombNode)
		}
	} else {
		if combnode {
			respService = wire.ServiceFlag(ServiceCombNodeClient)
		} else {
			respService = wire.ServiceFlag(ServiceCombClient)
		}
	}

	msgVersion := NewMsgVersionFromConn(conn, nonce, int32(myCount()), respService)
	msgVersion.DisableRelayTx = true
	msgVersion.UserAgent = "/"
	if err := msgVersion.AddUserAgent(UserAgentName, UserAgentVersion); err != nil {
		log.Println(err)
		return err
	}
	if err := wire.WriteMessage(conn, msgVersion, pver, btcnet); err != nil {
		log.Println(err)
		return err
	}

	log.Printf("Version message sent to %s: %d\n", f.Addr, pver)
outer:
	for {
		// Read the response version.
		msg, _, err := wire.ReadMessage(conn, pver, btcnet)
		if minBullshitMessagesAcceptedVersion && err != nil && err.Error() == "received unknown message" {
			// in case someone will send us an unknown message in the initial loop
			err = nil
		}
		if err != nil {
			log.Println(err)
			return err
		}

		switch vmsg := msg.(type) {
		case *wire.MsgVersion:

			log.Printf("Received MsgVersion: %T", vmsg)
			// Negotiate protocol version.
			if uint32(vmsg.ProtocolVersion) < pver {
				pver = uint32(vmsg.ProtocolVersion)
			}

			if combnode && (uint64(vmsg.Services) != ServiceCombNodeBloomClient) {
				log.Printf("Disconnecting %s: %d: No comb node (comb node forced)\n", f.Addr, pver)
				return errCombNodeForced
			} else if combnode {
				log.Printf("Found comb node %s: %d: (yay)\n", f.Addr, pver)
			}

			if ConfigForceBloom {
				if syncheaders {
					if vmsg.Services&wire.SFNodeBloom == 0 {
						log.Printf("Staying connected to %s: %d: Regardless of no bloom (bloom forced)\n", f.Addr, pver)
					}
				} else if vmsg.Services&wire.SFNodeBloom == 0 {
					BanRoamingNode(f.Addr)

					log.Printf("Banning %s: %d: No bloom (bloom forced)\n", f.Addr, pver)
					return errBloomForced
				}
			}
			if vmsg.Services == wire.ServiceFlag(ServiceCombNodeBloomClient) {
				extraBits |= FullNodeAcceleratorBloom
			} else {
				if vmsg.Services&wire.SFNodeBloom != 0 {
					extraBits |= FullNodeDownloadable | FullNodeDownloadableAndBloomable
				} else {
					extraBits |= FullNodeDownloadable
				}
				if vmsg.Services&wire.SFNodeWitness != 0 {
					extraBits |= FullNodeSegDownloadable
				}
			}

		case *wire.MsgAddrV2:

			if ConfigHaveExtranodes() {
				for _, addr := range vmsg.AddrList {

					if is_bad_port(addr.Port) {
						continue
					}

					if ConfigForceBloom && addr.Services&wire.SFNodeBloom == 0 {
						continue
					}

					if addr.IsTorV3() {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else if isIpV6(addr.Addr.String()) {
						AddRoamingNode(fmt.Sprintf("[%s]:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					}
				}
			}

		case *wire.MsgVerAck:

			log.Printf("Version ack received from %s: %d\n", f.Addr, pver)
			if minBullshitMessagesAcceptedVersion {
				WiringMutex.Lock()
				err = func() error {
					// Send verack.
					if err := wire.WriteMessage(conn, wire.NewMsgSendAddrV2(), pver, btcnet); err != nil {
						log.Println(err)
						return err
					}
					return nil
				}()
				WiringMutex.Unlock()
				if err != nil {
					log.Println(err)
					return err
				}
			}
			break outer

		case *wire.MsgSendAddrV2:
			if minBullshitMessagesAcceptedVersion {
				msgAddrV2 := wire.NewMsgAddrV2()

				for _, nodstr := range ConfigGossip {

					//println(nodstr)
					addr := ToNetString(nodstr)

					if addr == nil {
						continue
					}

					//println(addr.String())
					msgAddrV2.AddrList = append(msgAddrV2.AddrList, wire.NetAddressV2FromBytes(time.Now(),
						wire.ServiceFlag(ServiceCombNode), addr.Bytes, addr.Port))
				}
				if len(msgAddrV2.AddrList) > 0 {
					WiringMutex.Lock()
					_ = func() error {
						if err := wire.WriteMessage(conn, msgAddrV2, pver, btcnet); err != nil {
							log.Println(err)
							return err
						}
						return nil
					}()
					WiringMutex.Unlock()
				}
			}
		default:
			if vmsg != nil {
				log.Printf("Did not receive version message: %T", vmsg)
			}
			_ = vmsg
			continue
		}

	}
	WiringMutex.Lock()
	err = func() error {
		// Send verack.
		if err := wire.WriteMessage(conn, wire.NewMsgVerAck(), pver, btcnet); err != nil {
			log.Println(err)
			return err
		}

		locatorHash, chainBlocks := myTop()

		if chainBlocks < f.checkpointHeight {
			f.checkpointHeight = 0
		}

		newHdrs := wire.NewMsgGetHeaders()

		newHdrs.ProtocolVersion = pver
		if err := newHdrs.AddBlockLocatorHash(locatorHash); err != nil {
			log.Println(err)
			return err
		}

		if err := wire.WriteMessage(conn, newHdrs, pver, btcnet); err != nil {
			log.Println(err)
			return err
		}
		return nil
	}()
	WiringMutex.Unlock()
	if err != nil {
		log.Println(err)
		return err
	}
	var headerSyncOver, headersForkOver, headersNoSpam bool

	if syncheaders {
		defer myDeactivateChain()
		PutFullNode(f, FullNodePingable)
		defer DeleteFullNode(f)
	} else {
		PutFullNode(f, FullNodePingable|extraBits)
		defer DeleteFullNode(f)
	}

	var lastPrint chainhash.Hash
	var transactions []chainhash.Hash
	var merkleBlock *Block
	var maxPull = 1
	var wanted_block [32]byte
	// Listen for tx inv messages.
	for {
		if ConfigHaveExtranodes() && MapFewNodes() {
			err := f.GetNodes()
			if err != nil {
				log.Println(err)
			}
		}
		_, msg, _, err := wire.ReadMessageWithEncodingN(conn, pver, btcnet, wire.WitnessEncoding)
		if minBullshitMessagesAcceptedVersion && err != nil && err.Error() == "received unknown message" {
			// in case someone will send us an unknown message in the main loop
			err = nil
		}
		if err != nil && err.Error() == "ReadMessage: unhandled command [addrv2]" {
			// in case someone will build against old btcd / wire
			err = nil
		}

		if err != nil {
			log.Printf("Failed to read message: %v", err)
			if err == io.EOF {
				log.Println("It appears the remote node closed the connection.")
			}
			break
		}

		switch tmsg := msg.(type) {
		case *wire.MsgInv:
			//log.Printf("MsgInv received from %s\n", f.Addr)
			var is_new_block = false
			var is_new_block_twice = false

			for _, inv := range tmsg.InvList {
				if inv.Type == wire.InvTypeBlock {

					has := myHas(&inv.Hash)

					if !has {
						is_new_block_twice = wanted_block == inv.Hash
						is_new_block = true
						wanted_block = inv.Hash
					}

				}
			}

			if syncheaders {
				if is_new_block && headerSyncOver && !headersNoSpam {
					headersNoSpam = true

					err := get_headers_node(&conn, pver, btcnet, 1)
					if err != nil {
						log.Println(err)
					}

				} else if is_new_block && headerSyncOver && headersNoSpam {
					err := f.GetHeaders([32]byte{})
					if err != nil {
						log.Println(err)
					}
				} else if is_new_block_twice {

					err := get_headers_node(&conn, pver, btcnet, 0)
					if err != nil {
						log.Println(err)
					}
				}

			} else if headerSyncOver {
				if is_new_block {
					err := GetHeaderNode(wanted_block)
					if err != nil {
						log.Println(err)
					}
				}
			}

			continue
		case *wire.MsgPong:

			f.hadMut.Lock()
			f.kick = f.nonce != tmsg.Nonce
			f.hadMut.Unlock()

		case *wire.MsgPing:

			WiringMutex.Lock()

			if err := wire.WriteMessage(conn, wire.NewMsgPong(tmsg.Nonce), pver, btcnet); err != nil {

				WiringMutex.Unlock()

				log.Println(err)
				return err
			}

			WiringMutex.Unlock()

		case *wire.MsgHeaders:
			//log.Printf("MsgHeaders received from %s\n", f.Addr)

			if maxPull < len(tmsg.Headers) {
				maxPull = len(tmsg.Headers)
			}

			if is_nonstd, is_helpful := nonstandard_receive_accelerators(tmsg); is_nonstd {
				if is_helpful {
					RoamingNodeWasHelpful(f.Addr)
				}
				//println("got nonstandard accelerator")
				tmsg.Headers = nil
			}

			if syncheaders {

				var hasExisting = false
				var hasNew = false
				var hasPrev = false

				if len(tmsg.Headers) > 0 {

					prev := tmsg.Headers[0].PrevBlock

					lowblock := tmsg.Headers[0].BlockHash()
					highblock := tmsg.Headers[len(tmsg.Headers)-1].BlockHash()

					hasPrev = myHas(&prev)
					hasExisting = myHas(&lowblock)
					hasNew = !myHas(&highblock)

					f.hadMut.Lock()
					if f.newHdr != [32]byte{} {

						hasExisting = hasExisting && f.newHdr != lowblock
						hasNew = hasNew && f.newHdr == highblock

					}
					f.hadMut.Unlock()
				}

				if !hasNew && !hasExisting && !headersForkOver && (len(tmsg.Headers) > 0) {

					headersNoSpam = true

				} else if !hasNew && hasExisting && !headersForkOver && !headersNoSpam && hasPrev {

					myThrowAwayTopBlocks(uint64(len(tmsg.Headers)))

					err := get_headers_node(&conn, pver, btcnet, maxPull)
					if err != nil {
						log.Println(err)
					}

				} else {

					var oldStatus = myStatus(func(tot uint64, hsh *chainhash.Hash) {
						if *hsh != lastPrint {
							lastPrint = *hsh
							log.Printf("top header %d %s\n", tot, *hsh)
						}
					})

					for _, hdr := range tmsg.Headers {

						if !myAttach(hdr, myCheckpoint(), &f.checkpointHeight, func(ra, rb *chainhash.Hash, ha, hb uint64) {
							log.Printf("Reorg %d %s, %d %s\n", ha, *ra, hb, *rb)

							headersForkOver = true

						}) {
							log.Println("Received block does not have the required difficulty.")
							log.Printf("Previous block %s.\n", hdr.PrevBlock)
							log.Println("Your download cannot start because this may be a bcash node. Please try again with a bitcoin node.")
							return errBCash
						}

					}

					var newStatus = myStatus(func(tot uint64, hsh *chainhash.Hash) {
						if *hsh != lastPrint {
							lastPrint = *hsh
							log.Printf("tip header %d %s\n", tot, *hsh)
							go InvFullNodes(lastPrint, FullNodeUploader)
						}

					})

					if newStatus > oldStatus {

						err := get_headers_node(&conn, pver, btcnet, 0)
						if err != nil {
							log.Println(err)
						}

					} else if (!hasNew && (newStatus == oldStatus)) || (newStatus == ConfigMaxBlock) {

						headersForkOver = false
						headersNoSpam = false

						if !headerSyncOver {
							headerSyncOver = true

							if f.checkpointHeight == 0 && myCheckpoint() != nil {

								log.Printf("ERROR: Checkpoint not reached: %s\n", myCheckpoint())
								return errCheckpoint
							}
							if 1 == ConfigAddNodeCount() && !ConfigHaveExtranodes() {
								// no Add nodes, full node is add node too
								log.Println("allowing block access")
								DeleteFullNode(f)
								PutFullNode(f, FullNodePingable|FullNodeDownloadable|extraBits)
							}

							log.Println("activating chain")
							myActivateChain()
							if ConfigRpc {
								log.Println("enabling webserver")
								go Serve()
							}

							log.Println("inventory to nodes")
							go InvFullNodes(lastPrint, FullNodeUploader)
							var acceleratorHash = acceleratorUnhex(ConfigAcceleratorHash)

							go acceleratorOpen(ConfigAcceleratorsRw, ConfigAcceleratorsRo, acceleratorHash)

							go AddrFullNodes()
						}

					}

				}
			}
		case *wire.MsgVerAck:

			log.Printf("Version ack received from %s\n", f.Addr)

			continue
		case *wire.MsgAddr:
			if ConfigHaveExtranodes() {
				for _, addr := range tmsg.AddrList {

					if is_bad_port(addr.Port) {
						continue
					}

					if ConfigForceBloom && addr.Services&wire.SFNodeBloom == 0 {
						continue
					}

					if isIpV6(addr.IP.String()) {
						AddRoamingNode(fmt.Sprintf("[%s]:%d", addr.IP.String(), addr.Port), uint64(addr.Services))
					} else {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.IP.String(), addr.Port), uint64(addr.Services))
					}
				}
			}
			continue

		case *wire.MsgBlock:
			proxy_block(tmsg)
			if !ConfigRpc {
				break
			}

			var blk = new(Block)
			var bh = tmsg.Header.BlockHash()
			blk.Height = myBlkHeight(&bh)
			blk.Hash = bh.String()
			blk.PBH = tmsg.Header.PrevBlock.String()
			blk.pulled = time.Now()

			if ConfigUsecasePricecharts {
				blk.Time = tmsg.Header.Timestamp.Unix()
			}

			var merkle []chainhash.Hash

			for _, tx := range tmsg.Transactions {

				var hash = tx.TxHash()
				merkle = append(merkle, hash)

				var t Transaction

				if ConfigUsecasePricecharts || ConfigUsecaseAccelerator {

					t.TxId = [32]byte(hash)

					if ConfigUsecasePricecharts {

						for _, txin := range tx.TxIn {
							var i Input

							i.TxId = [32]byte(txin.PreviousOutPoint.Hash)
							i.Vout = txin.PreviousOutPoint.Index

							t.Vin = append(t.Vin, i)
						}

					}

				}

				for _, txout := range tx.TxOut {

					var o Output
					if ConfigUsecasePricecharts {
						o.Value = float64(txout.Value) / 100000000
					}
					if len(txout.PkScript) == 34 &&
						txout.PkScript[0] == 0 && txout.PkScript[1] == 0x20 {
						var buf [32]byte
						copy(buf[0:32], txout.PkScript[2:])

						o.ScriptPubKey = &SPubKey{
							Type: "witness_v0_scripthash",
							Hex:  buf,
						}
					}
					t.Vout = append(t.Vout, o)
				}
				blk.Tx = append(blk.Tx, t)
			}

			if merkleTree(merkle) != tmsg.Header.MerkleRoot {

				log.Printf("Received block %d merkle root is not valid\n", bh)
				return errBlkMerkleNotValid

			} else {

				var buf = bh
				for i := 0; i < 16; i++ {
					buf[i], buf[31-i] = buf[31-i], buf[i]
				}

				NotifyBlock(buf, blk)
				RoamingNodeWasHelpful(f.Addr)
			}

		case *wire.MsgAddrV2:
			if ConfigHaveExtranodes() {
				for _, addr := range tmsg.AddrList {

					if is_bad_port(addr.Port) {
						continue
					}

					if ConfigForceBloom && addr.Services&wire.SFNodeBloom == 0 {
						continue
					}

					if addr.IsTorV3() {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else if isIpV6(addr.Addr.String()) {
						AddRoamingNode(fmt.Sprintf("[%s]:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					} else {
						AddRoamingNode(fmt.Sprintf("%s:%d", addr.Addr.String(), addr.Port), uint64(addr.Services))
					}
				}
			}
		case *wire.MsgMerkleBlock:
			proxy_merkle(tmsg)
			if !ConfigRpc {
				break
			}

			var blk = new(Block)
			var bh = tmsg.Header.BlockHash()
			blk.Height = myBlkHeight(&bh)
			blk.Hash = bh.String()
			blk.PBH = tmsg.Header.PrevBlock.String()
			blk.pulled = time.Now()

			if ConfigUsecasePricecharts {
				blk.Time = tmsg.Header.Timestamp.Unix()
			}

			transactions = merkleFrontier(tmsg.Transactions, tmsg.Hashes, tmsg.Flags, tmsg.Header.MerkleRoot)

			if transactions == nil {

				if len(tmsg.Hashes) == 1 && *tmsg.Hashes[0] == tmsg.Header.MerkleRoot {

					for i := uint32(0); i < tmsg.Transactions; i++ {
						var t Transaction
						blk.Tx = append(blk.Tx, t)
					}

				} else {

					log.Printf("Received merkle block %d merkle root is not valid\n", bh)
					return errBlkMerkleNotValid
				}
			}

			var buf = bh
			for i := 0; i < 16; i++ {
				buf[i], buf[31-i] = buf[31-i], buf[i]
			}

			blk.hash = buf

			if transactions == nil {

				var HEIGHT = myBlkHeightOfBlock(buf)
				var COUNT = acceleratorCount(HEIGHT)

				if 0 != COUNT {
					log.Printf("Received nonempty merkle block with %d transactions, %d p2wsh, height %d, expected empty\n", tmsg.Transactions, COUNT, HEIGHT)
					continue
				}

				NotifyBlock(blk.hash, blk)
				RoamingNodeWasHelpful(f.Addr)
				continue
			}

			for (len(transactions) > 0) && (transactions[0] == chainhash.Hash{}) {
				blk.Tx = append(blk.Tx, Transaction{})
				transactions = transactions[1:]
			}

			merkleBlock = blk

		case *wire.MsgTx:
			proxy_tx(tmsg)
			if !ConfigRpc {
				break
			}
			if merkleBlock != nil && len(transactions) > 0 {
				var hash = tmsg.TxHash()

				if hash == transactions[0] {
					transactions = transactions[1:]

					var t Transaction

					if ConfigUsecaseAccelerator {

						t.TxId = [32]byte(hash)

					}

					for _, txout := range tmsg.TxOut {

						var o Output
						if len(txout.PkScript) == 34 &&
							txout.PkScript[0] == 0 && txout.PkScript[1] == 0x20 {
							var buf [32]byte
							copy(buf[0:32], txout.PkScript[2:])

							o.ScriptPubKey = &SPubKey{
								Type: "witness_v0_scripthash",
								Hex:  buf,
							}
						}
						t.Vout = append(t.Vout, o)
					}
					merkleBlock.Tx = append(merkleBlock.Tx, t)

					for (len(transactions) > 0) && (transactions[0] == chainhash.Hash{}) {
						merkleBlock.Tx = append(merkleBlock.Tx, Transaction{})
						transactions = transactions[1:]
					}

					if len(transactions) == 0 {

						var ACTUALCOUNT = merkleBlock.P2WSHCount()
						var HEIGHT = myBlkHeightOfBlock(merkleBlock.hash)
						var EXCPECTEDCOUNT = acceleratorCount(HEIGHT)

						if ACTUALCOUNT != EXCPECTEDCOUNT {
							if EXCPECTEDCOUNT != ^uint64(0) {
								log.Printf("Received different p2wsh counted merkle block, %d actual, %d expected, at height %d\n", ACTUALCOUNT, EXCPECTEDCOUNT, HEIGHT)
							}
						} else {
							NotifyBlock(merkleBlock.hash, merkleBlock)
							RoamingNodeWasHelpful(f.Addr)
						}
						merkleBlock = nil
						transactions = nil
					}
				} else {
					merkleBlock = nil
					transactions = nil
				}
			} else {
				merkleBlock = nil
				transactions = nil
			}
		case *wire.MsgGetHeaders:

			err, _ := nonstandard_accelerator_headers_do(conn, tmsg, pver, btcnet)
			if err != nil {
				log.Println(err)
				return err
			}

			//if ConfigIsUploader() {
			//	headersHeight, _ = generic_headers_height(tmsg)
			//	needsHeaders = errChainInactive == generic_headers_delivery(conn, headersHeight, pver, btcnet)
			//}
		case *wire.MsgGetAddr:

			msgAddrV2 := wire.NewMsgAddr()

			for _, nodstr := range ConfigGossip {

				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				if net.ParseIP(addr.String()) == nil {
					continue
				}
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, &wire.NetAddress{
					Timestamp: time.Now(),
					Services:  wire.ServiceFlag(ServiceCombNode),
					IP:        net.ParseIP(addr.String()),
					Port:      addr.Port,
				})
			}

			var prevstr string

			for i := 0; i < 1000-len(ConfigGossip); i++ {
				servenod, nodstr := MapRandomNode()
				if servenod == nil {
					continue
				}
				if nodstr == prevstr {
					break
				}

				prevstr = nodstr
				//println(nodstr)
				addr := ToNetString(nodstr)
				if addr == nil {
					continue
				}

				//println(addr.String())

				if net.ParseIP(addr.String()) == nil {
					continue
				}

				msgAddrV2.AddrList = append(msgAddrV2.AddrList, &wire.NetAddress{
					Timestamp: time.Now(),
					Services:  wire.ServiceFlag(servenod.Services),
					IP:        net.ParseIP(addr.String()),
					Port:      addr.Port,
				})
			}

			if len(msgAddrV2.AddrList) > 0 {

				WiringMutex.Lock()
				_ = func() error {
					if err := wire.WriteMessage(conn, msgAddrV2, pver, btcnet); err != nil {
						log.Println(err)
						return err
					}
					return nil
				}()
				WiringMutex.Unlock()

			}
		case *wire.MsgSendAddrV2:

			msgAddrV2 := wire.NewMsgAddrV2()

			for _, nodstr := range ConfigGossip {

				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, wire.NetAddressV2FromBytes(time.Now(),
					wire.ServiceFlag(ServiceCombNode), addr.Bytes, addr.Port))
			}

			var prevstr string

			for i := 0; i < 1000-len(ConfigGossip); i++ {
				servenod, nodstr := MapRandomNode()
				if servenod == nil {
					continue
				}
				if nodstr == prevstr {
					break
				}

				prevstr = nodstr
				//println(nodstr)
				addr := ToNetString(nodstr)

				if addr == nil {
					continue
				}

				//println(addr.String())
				msgAddrV2.AddrList = append(msgAddrV2.AddrList, wire.NetAddressV2FromBytes(time.Now(),
					wire.ServiceFlag(servenod.Services), addr.Bytes, addr.Port))
			}
			if len(msgAddrV2.AddrList) > 0 {
				WiringMutex.Lock()
				_ = func() error {
					if err := wire.WriteMessage(conn, msgAddrV2, pver, btcnet); err != nil {
						log.Println(err)
						return err
					}
					return nil
				}()
				WiringMutex.Unlock()
			}
		case *wire.MsgFilterLoad:

			//println("load", len(tmsg.Filter))

			err := nonstandard_bloom_filter_do(conn, tmsg, pver, btcnet)
			if err != nil {
				log.Println(err)
				return err
			}

			if tmsg.HashFuncs == 0 && tmsg.Tweak == 0 && tmsg.Flags == 0 && len(tmsg.Filter) >= 40 {
				NotifyBloom(tmsg.Filter[0:40], tmsg.Filter[40:])
				RoamingNodeWasHelpful(f.Addr)
			}
		case *wire.MsgGetData:

			msgNotFound := wire.NewMsgNotFound()
			msgNotFound.InvList = tmsg.InvList

			WiringMutex.Lock()
			_ = func() error {
				if err := wire.WriteMessage(conn, msgNotFound, pver, btcnet); err != nil {
					log.Println(err)
					return err
				}
				return nil
			}()
			WiringMutex.Unlock()

		default:
			//log.Printf("Unknown msg: %T", tmsg)
			_ = tmsg
			continue
		}
	}

	return nil
}

func isIpV6(ip string) bool {
	for i := 0; i < len(ip); i++ {
		switch ip[i] {
		case '.':
			return false
		case ':':
			return true
		}
	}
	return true
}

func isTor(ip string) bool {
	return strings.Contains(ip, ".onion:")
}

func (fn *FullNode) GetNodes() error {
	ttime := time.Now().Add(time.Second * -1)
	if ttime.After(fn.getnod) {
		fn.getnod = time.Now()
		return getnodes_node(fn.conn, fn.pver, fn.btcnet)
	}
	return nil
}

func getnodes_node(conn *net.Conn, pver uint32, btcnet wire.BitcoinNet) error {

	WiringMutex.Lock()
	defer WiringMutex.Unlock()

	//println("wiring getnode")
	msg := wire.NewMsgGetAddr()
	return wire.WriteMessage(*conn, msg, pver, btcnet)

}

func nonstandard_receive_accelerators(tmsg *wire.MsgHeaders) (is_nonstandard_packet, is_helpful bool) {
	if len(tmsg.Headers) == 0 {
		return false, false
	}
	var accelerators []Accelerator
	for _, acc := range tmsg.Headers {
		if acc.Version != 0 {
			return false, false
		}
		if acc.Timestamp != time.Unix(0, 0) {
			return false, false
		}
		var accel = Accelerator{
			Hash:      [32]byte(acc.MerkleRoot),
			BlockHash: [32]byte(acc.PrevBlock),
			Count:     uint64(acc.Bits)<<32 | uint64(acc.Nonce),
		}
		accelerators = append(accelerators, accel)
	}
	for i := 0; i < len(accelerators)/2; i++ {
		accelerators[i], accelerators[len(accelerators)-1-i] = accelerators[len(accelerators)-1-i], accelerators[i]
	}
	var finalhash [32]byte
	for i, accel := range accelerators {
		var hash = accel.Sha()
		if i+1 == len(accelerators) {
			finalhash = hash
		} else {
			var accelNext = accelerators[i+1]
			if accelNext.Hash != hash {
				return true, false
			}
		}
	}
	NotifyAccelerators(finalhash, accelerators)
	return true, len(accelerators) > 1
}