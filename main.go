package main

import "github.com/btcsuite/btcd/chaincfg/chainhash"
import "log"
import "os"
import "fmt"
import "flag"
import "strings"
import "time"

const localHostNode = "127.0.0.1:8333"
const localHostTestNode = "127.0.0.1:18333"
const localHostRegTestNode = "127.0.0.1:18444"
const localTorNode = "127.0.0.1:9150"

func ConfigMyBtcFullNode(i int) (node string) {
	if len(ConfigBtcFullNode) == 0 {
		if ConfigRegtest {
			return localHostRegTestNode
		}
		if ConfigTestnet() {
			return localHostTestNode
		}
		return localHostNode
	}
	return ConfigBtcFullNode[i%len(ConfigBtcFullNode)]
}
func ConfigTor() (node string) {
	node = os.Getenv("TORIPPORT")
	if len(node) == 0 {
		return localTorNode
	}
	return node
}
func ConfigCheckpoint() (node string) {
	node = os.Getenv("BTCCHECKPOINT")
	if len(node) == 0 {

		if ConfigTestnet() {
			//2,000,000
			return "000000000000010dd0863ec3d7a0bae17c1957ae1de9cbcdae8e77aad33e3b8c"
		}

		if ConfigRegtest {

			return ""

		}

		//712,235
		return "00000000000000000000189551f66e2c0d2ac226c3dad5207788500d87bdce2d"

	}
	return node
}
func ConfigAddNodeCount() (count int) {
	for n := 1; true; n++ {
		if len(os.Getenv(fmt.Sprintf("ADDNODE%d", n))) == 0 {
			return int(n)
		}
	}
	return 1
}

func ConfigAddNode(n int) (node string) {
	node = os.Getenv(fmt.Sprintf("ADDNODE%d", n))
	if len(node) == 0 {
		return ConfigMyBtcFullNode(0)
	}
	return node
}

func ConfigHaveExtranodes() (nodes bool) {
	return len(os.Getenv("HaveEXTRANODES")) != 0
}
func ConfigForceTor() (nodes bool) {
	return len(os.Getenv("FORCETOR")) != 0
}

//func ConfigIsUploader() (isUploader bool) {
//	return len(os.Getenv("UPLOADERIPPORT")) != 0
//}

func ConfigTestnet() (testnet bool) {
	return len(os.Getenv("TESTNET")) != 0
}

func ConfigIsBloom() (isBloom bool) {
	if len(ConfigAcceleratorsRo) > 0 {
		return true
	}
	if len(ConfigAcceleratorsRw) > 0 {
		return true
	}
	return len(ConfigAcceleratorHash) == 64
}

// global, cannot change at runtime
var ConfigAcceleratorHash string

// global, cannot change at runtime
var ConfigAcceleratorsRo addnodeFlags

// global, cannot change at runtime
var ConfigAcceleratorsRw addnodeFlags

// global, cannot change at runtime
var ConfigBtcFullNode addnodeFlags

// global, cannot change at runtime
var ConfigGossip addnodeFlags

// global, cannot change at runtime
var ConfigForceBloom bool

// global, cannot change at runtime
var ConfigUsecaseAccelerator bool

// global, cannot change at runtime
var ConfigUsecasePricecharts bool

// global, cannot change at runtime
var ConfigRegtest bool

// global, cannot change at runtime
var ConfigRecycleHeight int

// global, cannot change at runtime
var ConfigMinerDifficulty int
var ConfigMinerFunctions int

// global, cannot change at runtime
var ConfigMaxBlock uint64

// global, cannot change at runtime
var ConfigRpc bool

// global, cannot change at runtime
var ConfigTimeout int

// global, cannot change at runtime
var ConfigAccCap, ConfigBannedNodesCap, ConfigNodesCap int

type addnodeFlags []string

func (i *addnodeFlags) String() string {
	return "???"
}

func (i *addnodeFlags) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}

const AppVersion = "0.0.13"

func main() {
	var checkpoint, tor, uploader, regtest string
	var addnodes addnodeFlags
	var extranodes, combnodes, reorg int
	var forcetor, testnet, ver, nested bool
	flag.Var(&ConfigBtcFullNode, "my-btc-full-node", "My btc full node provides reference about"+
		" the longest headers chain to sync as well as block download. Defaults to "+localHostNode)
	flag.Var(&addnodes, "addnode", "Add block puller node.")
	flag.StringVar(&ConfigAcceleratorHash, "accelerator", "", "Accelerator hash. Works like a checkpoint.")
	flag.Var(&ConfigAcceleratorsRo, "add-file-ro", "Add accelerator file READ ONLY.")
	flag.Var(&ConfigAcceleratorsRw, "add-file-rw", "Add accelerator file READ WRITE.")
	flag.Var(&ConfigGossip, "gossip", "Add gossiped node ip:port.")
	flag.Var(&addnodes, "add-node", "Add block puller node.")
	flag.IntVar(&extranodes, "extra-nodes", 0, "Count of extra online (free roaming) nodes to add using auto discovery.")
	flag.IntVar(&combnodes, "comb-nodes", 0, "Count of comb online (accelerator) nodes to add using auto discovery.")
	flag.IntVar(&ConfigMinerDifficulty, "miner-difficulty", 0, "Difficulty of mining (0=disabled).")
	flag.IntVar(&ConfigMinerFunctions, "miner-functions", 0, "Mining functions used (7=recommended).")
	flag.IntVar(&ConfigTimeout, "timeout", 120, "Timeout (seconds). Ping and request headers every N seconds, and detect stall on the BTC node (when busy syncing).")
	flag.IntVar(&ConfigRecycleHeight, "recycle-headers", -1, "Recycle headers at this height/depth when reconnecting to my node.")
	flag.IntVar(&reorg, "reorg", -1, "Don't advance beyond a specific block height. Useful to reorg the client.")
	flag.IntVar(&ConfigAccCap, "acc-cap", -1, "Capacity of accelerator size request map.")
	flag.IntVar(&ConfigBannedNodesCap, "bannednodes-cap", -1, "Capacity of banned nodes map.")
	flag.IntVar(&ConfigNodesCap, "nodes-cap", -1, "Capacity of known ips nodes map.")
	flag.StringVar(&checkpoint, "checkpoint", "", "Bitcoin block hash checkpoint.")
	flag.StringVar(&regtest, "regtest", "", "Bitcoin regtest genesis hash.")
	flag.StringVar(&tor, "tor", "", "Tor ip:port. Defaults to "+localTorNode)
	flag.StringVar(&uploader, "uploader", "", "Uploader ip:port. Disabled if not set.")
	flag.BoolVar(&ConfigUsecasePricecharts, "usecase-pricecharts", false, "Enable usecase PriceCharts.")
	flag.BoolVar(&ConfigUsecaseAccelerator, "usecase-accelerator", false, "Enable usecase Accelerator.")
	flag.BoolVar(&forcetor, "force-tor", false, "Force tor for all connections.")
	flag.BoolVar(&ConfigForceBloom, "force-bloom", false, "Force bloom (accelerators) for all connections.")
	flag.BoolVar(&testnet, "testnet", false, "Sync with testnet.")
	flag.BoolVar(&nested, "nested", false, "Nested mode, doesn't run RPC.")
	flag.BoolVar(&ver, "v", false, "Print version and exit.")
	flag.Parse()

	if ver {
		fmt.Println(AppVersion)
		os.Exit(0)
	}
	if miner_should_be_running() {
		ConfigUsecaseAccelerator = true
	}

	ConfigMaxBlock = uint64(int64(reorg))

	ConfigRpc = !nested || len(uploader) == 0 || miner_should_be_running()

	if len(checkpoint) > 0 {
		os.Setenv("BTCCHECKPOINT", checkpoint)
	}

	for _, addnode := range addnodes {
		os.Setenv(fmt.Sprintf("ADDNODE%d", ConfigAddNodeCount()), addnode)
	}
	if extranodes+combnodes > 0 {
		os.Setenv("HaveEXTRANODES", "yes")
	}

	if len(tor) > 0 {
		os.Setenv("TORIPPORT", tor)
	}
	if len(uploader) > 0 {
		os.Setenv("UPLOADERIPPORT", uploader)
	}
	if forcetor {
		os.Setenv("FORCETOR", "yes")
	}
	if testnet {
		os.Setenv("TESTNET", "yes")
	}

	// insert Basic Hashes
	// Block 0 hash.
	hashStr := "000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f"
	if testnet {
		hashStr = "000000000933ea01ad0ee984209779baaec3ced90fa3f408719526f8d77f4943"
	}
	if len(regtest) == 64 {
		ConfigRegtest = true
		hashStr = regtest
	}
	locatorHash, err := chainhash.NewHashFromStr(hashStr)
	if err != nil {
		log.Fatalf("Genesis hash error: %v", err)
	}
	myPut(locatorHash, 0)

	for j := 1; j < ConfigAddNodeCount(); j++ {
		var fn FullNode
		fn.Addr = ConfigAddNode(j)
		go fn.RunForever(false)
	}

	go func() {
		for {
			GcBlocks()
			AddrFullNodes()
			SometimesHalveBanTable()
			if err := PingNodes(); err != nil {
				log.Println(err)
			}
			time.Sleep(time.Duration(ConfigTimeout/2) * time.Second)
			if err := GetHeadersNode(); err != nil {
				log.Println(err)
			}
			time.Sleep(time.Duration(ConfigTimeout/2) * time.Second)
		}
	}()

	for j := 0; j < extranodes; j++ {

		go RunForeverRoaming(false)
	}
	for j := 0; j < combnodes; j++ {

		go RunForeverRoaming(true)
	}
	if len(uploader) > 0 {
		myServer()
		go uploader_main(uploader)
	}

	var fn FullNode
	fn.RunForever(true)
}